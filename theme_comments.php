<?php



/******************************************************************************
** Section <<<$template_add_your_comment>>> - START
******************************************************************************/
$template_add_your_comment = <<<EOT
  
       <form method="post" name="post" id="post" action="db_input.php" class="form-horizontal span6">
          <strong>{ADD_YOUR_COMMENT}:{HELP_ICON}</strong>
<!-- BEGIN user_name_input -->
       <div class="control-group">
          <label class="control-label" for="msg_author">{USER_NAME}</label>
          <div class="controls">
            
            <input type="text" placeholder="{USER_NAME}" class="textinput span" name="msg_author" size="10" maxlength="20" value="{USER_NAME}" onclick="if (this.value == '{DEFAULT_USERNAME}') this.value = '';" onkeyup="if (this.value == '{DEFAULT_USERNAME}') this.value = '';" />
          </div>
        </div>
<!-- END user_name_input -->
<!-- BEGIN input_box_smilies -->
        <div class="control-group">
          <label class="control-label" for="message">{COMMENT}</label>
          <div class="controls">
            
             <textarea type="text"  class="textinput span" id="message" name="msg_body" onselect="storeCaret_post(this);" onclick="storeCaret_post(this);" onkeyup="storeCaret_post(this);" maxlength="{MAX_COM_LENGTH}" style="" ></textarea>
              {SMILIES}                  
          </div>
        </div>

<!-- END input_box_smilies -->
<!-- BEGIN smilies -->

<!-- END smilies -->
<!-- BEGIN input_box_no_smilies -->
                               
                                {COMMENT}
                        
                                <textarea type="text" class="textinput" id="message" name="msg_body"  maxlength="{MAX_COM_LENGTH}" style="width: 100%;" ></textarea>
                               
<!-- END input_box_no_smilies -->
<!-- BEGIN submit -->
        <div class="control-group">
          <label class="control-label" for="submit"></label>
          <div class="controls">
            
                <button type="submit" class="button btn btn-primary span4" name="submit" value="{OK}"  onclick="return notDefaultUsername(this.form, '{DEFAULT_USERNAME}', '{DEFAULT_USERNAME_MESSAGE}');">{OK_ICON}{OK}</button>
            </div>
        </div>                        
                                <input type="hidden" name="event" value="comment" />
                                <input type="hidden" name="pid" value="{PIC_ID}" />
                                
                                <input type="hidden" name="form_token" value="{FORM_TOKEN}" />
                                <input type="hidden" name="timestamp" value="{TIMESTAMP}" />

                                <!-- END submit -->
                                                        
<!-- BEGIN comment_captcha -->
              <div class="control-group">
                <label class="control-label" for="message"> {CONFIRM} </label>
                <div class="controls">
                   <img src="captcha.php?theme=sample" align="middle" border="0" alt="" />
                   <input type="text" name="confirmCode" size="5" maxlength="5" class="textinput" />
                                                     
                </div>

<!-- END comment_captcha -->
                 </div>

<!-- BEGIN login_to_comment -->
        <div class="fluid-row">
                                
                                  {LOGIN_TO_COMMENT}
                               
        </div>
<!-- END login_to_comment -->
                
        </form>
EOT;
/******************************************************************************
** Section <<<$template_add_your_comment>>> - END
******************************************************************************/



/******************************************************************************
** Section <<<$template_image_comments>>> - START
******************************************************************************/
// HTML template for the display of comments
$template_image_comments = <<<EOT

	<div class="row-fluid comment">
        <div class="row-fluid comment-meta">
            <strong class="span6 text-left comment-autor">{MSG_AUTHOR_LNK}</strong>  <div class="span6 text-right"><span class="comment-date">{MSG_DATE}</span></div>
            <a name="comment{MSG_ID}"></a>&nbsp;

            <div id="cedit{MSG_ID}" style="display:none"></div>
        </div>
        <div class="row-fluid text-justify comment-body" id="cbody{MSG_ID}" style="display:block">
               <div class="span12 "> {MSG_BODY}</div>
        </div>
<!-- BEGIN ipinfo -->
                        ({IP})
<!-- END ipinfo -->
                    
<!-- BEGIN report_comment_button -->
                        <a href="report_file.php?pid={PID}&amp;msg_id={MSG_ID}&amp;what=comment" title="{REPORT_COMMENT_TITLE}">{REPORT_COMMENT_ICON}</a>
<!-- END report_comment_button -->
<!-- BEGIN pending approval -->
                        {PENDING_APPROVAL}
<!-- END pending approval -->
<!-- BEGIN buttons -->
                        <script type="text/javascript">
                            document.write('<a href="javascript:;" onclick="blocking(\'cbody{MSG_ID}\',\'\', \'block\'); blocking(\'cedit{MSG_ID}\',\'\', \'block\'); return false;" title="{EDIT_TITLE}">{EDIT_ICON}</a>');
                        </script>
                        <a href="delete.php?msg_id={MSG_ID}&amp;what=comment&amp;form_token={FORM_TOKEN}&amp;timestamp={TIMESTAMP}" onclick="return confirm('{CONFIRM_DELETE}');" title="{DELETE_TITLE}">{DELETE_ICON}</a>
<!-- END buttons -->

                
    <tr>
        <td class="tableb tableb_alternate">
           
<!-- BEGIN edit_box_smilies -->
                <form name="f{MSG_ID}" id="f{MSG_ID}" method="post" action="db_input.php" class="span12">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top">
                                <input type="hidden" name="event" value="comment_update" />
                                <input type="hidden" name="msg_id" value="{MSG_ID}" />
                            </td>
                        </tr>
                        <tr>
                            <td width="80%">
                                <textarea cols="40" rows="2" class="textinput span8" name="msg_body" onselect="storeCaret_f{MSG_ID}(this);" onclick="storeCaret_f{MSG_ID}(this);" onkeyup="storeCaret_f{MSG_ID}(this);" >{MSG_BODY_RAW}</textarea>
                            </td>
                            <td class="tableb tableb_alternate"></td>
                           
                        </tr>
                        <tr>
                             <td>
                                <input type="submit" class="button" name="submit" value="{OK}" />
                                <input type="hidden" name="form_token" value="{FORM_TOKEN}" />
                                <input type="hidden" name="timestamp" value="{TIMESTAMP}" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <img src="images/spacer.gif" width="1" height="2" border="0" alt="" />
                                <br />
                            </td>
                        </tr>
                    </table>
                </form>
                {SMILIES}
<!-- END edit_box_smilies -->
<!-- BEGIN edit_box_no_smilies -->
                <form name="f{MSG_ID}" id="f{MSG_ID}" method="POST" action="db_input.php">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>    
                                <input type="hidden" name="event" value="comment_update" />
                                <input type="hidden" name="msg_id" value="{MSG_ID}" />
                            </td>
                        </tr>
                        <tr>
                            <td width="100%">
                                <textarea cols="40" rows="2" class="textinput" name="msg_body" style="width: 100%;">{MSG_BODY_RAW}</textarea>
                            </td>
                            <td class="tableb tableb_alternate"></td>
                            <td>
                                <input type="submit" class="button" name="submit" value="{OK}" />
                                <input type="hidden" name="form_token" value="{FORM_TOKEN}" />
                                <input type="hidden" name="timestamp" value="{TIMESTAMP}" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <img src="images/spacer.gif" width="1" height="2" border="0" alt="" />
                                <br />
                            </td>
                        </tr>
                    </table>
                </form>
<!-- END edit_box_no_smilies -->
            </div>
EOT;
/******************************************************************************
** Section <<<$template_image_comments>>> - END
******************************************************************************/



/******************************************************************************
** Section <<<theme_html_comments>>> - START
******************************************************************************/
// Displays comments for a specific picture
function theme_html_comments($pid)
{
    global $CONFIG, $USER, $CURRENT_ALBUM_DATA, $lang_date, $HTML_SUBST;
    global $template_image_comments, $template_add_your_comment, $lang_display_comments, $lang_common, $REFERER, $lang_bbcode_help_title, $lang_bbcode_help;

    $superCage = Inspekt::makeSuperCage();
    $template_add_your_comment = CPGPluginAPI::filter('theme_add_comment', $template_add_your_comment);
    $template_image_comments = CPGPluginAPI::filter('theme_edit_comment', $template_image_comments);

    list($timestamp, $form_token) = getFormToken();

    $html = '';

    //report to moderator buttons
    if (!(($CONFIG['report_post']==1) && (USER_CAN_SEND_ECARDS))) {
        template_extract_block($template_image_comments, 'report_comment_button');
    }

    if (!$CONFIG['enable_smilies']) {
        $tmpl_comment_edit_box = template_extract_block($template_image_comments, 'edit_box_no_smilies', '{EDIT}');
        template_extract_block($template_image_comments, 'edit_box_smilies');
        template_extract_block($template_add_your_comment, 'input_box_smilies');
    } else {
        $tmpl_comment_edit_box = template_extract_block($template_image_comments, 'edit_box_smilies', '{EDIT}');
        template_extract_block($template_image_comments, 'edit_box_no_smilies');
        template_extract_block($template_add_your_comment, 'input_box_no_smilies');
    }


    $tmpl_comments_buttons = template_extract_block($template_image_comments, 'buttons', '{BUTTONS}');
    $tmpl_comments_ipinfo = template_extract_block($template_image_comments, 'ipinfo', '{IPINFO}');

    if ($CONFIG['comments_sort_descending'] == 1) {
        $comment_sort_order = 'DESC';
    } else {
        $comment_sort_order = 'ASC';
    }

    $result = cpg_db_query("SELECT COUNT(msg_id) FROM {$CONFIG['TABLE_COMMENTS']} WHERE pid='$pid'");
    list($num) = mysql_fetch_row($result);

    if ($num) {

        $limit = $CONFIG['comments_per_page'];
        $max = ceil($num/$limit);

        if ($superCage->get->keyExists('page')) {
            $page = $superCage->get->getInt('page');
            $page = min($page, $max);
            $page = max(0, $page);
        } else if ($CONFIG['comments_sort_descending'] == 1) {
            $page = 1;
        } else {
            $page = $max;
        }

        $start = max(0, $num - (($max-($page-1))*$limit));

        ob_start();
        echo '<br />';
        starttable();


        echo '<tr><td class="tableh2"><div style="float: left">'.$lang_display_comments['comment'].' '.sprintf($lang_display_comments['comment_x_to_y_of_z'], ($start+1), min($num, $start+$limit), $num).'</div>';
        echo '<div style="float: right">'.$lang_display_comments['page'].': ';
        $links = array();

        for ($i = 1; $i <= $max; $i++){
            if ($i < 5 || ($i > $max - 5) || (($i > $page -5) && ($i < $page + 5))){
                $links[$i]= '<a href="displayimage.php?pid=' . $pid . '&amp;page='.$i.'#comments_top">'.$i.'</a>';
            }
        }

        $links[$page] = "<b>$page</b>";
        echo implode(' - ', $links);
        echo '</div></td></tr>';

        endtable();
        echo '<br />';
        $html .= ($tabs = ob_get_clean());

        $result = cpg_db_query("SELECT msg_id, msg_author, msg_body, UNIX_TIMESTAMP(msg_date) AS msg_date, author_id, author_md5_id, msg_raw_ip, msg_hdr_ip, pid, approval FROM {$CONFIG['TABLE_COMMENTS']} WHERE pid='$pid' ORDER BY msg_id $comment_sort_order LIMIT $start, $limit");

        while ($row = mysql_fetch_assoc($result)) { // while-loop start
            $user_can_edit = (GALLERY_ADMIN_MODE) || (USER_ID && USER_ID == $row['author_id'] && USER_CAN_POST_COMMENTS) || (!USER_ID && USER_CAN_POST_COMMENTS && ($USER['ID'] == $row['author_md5_id']));
            if (($user_can_edit != '' && $CONFIG['comment_user_edit'] != 0) || (GALLERY_ADMIN_MODE)) {
                $comment_buttons = $tmpl_comments_buttons;
                $comment_edit_box = $tmpl_comment_edit_box;
            } else {
                $comment_buttons = '';
                $comment_edit_box = '';
            }
            $comment_ipinfo = ($row['msg_raw_ip'] && GALLERY_ADMIN_MODE)?$tmpl_comments_ipinfo : '';
            $hide_comment = 0;

            // comment approval
            $pending_approval = '';
            if (USER_IS_ADMIN) {
                //display the selector approve/disapprove
                if ($row['approval'] == 'NO') {
                    $pending_approval = cpg_fetch_icon('comment_disapprove_disabled', 0) . '<a href="reviewcom.php?pos=-{PID}&amp;msg_id={MSG_ID}&amp;form_token={FORM_TOKEN}&amp;timestamp={TIMESTAMP}&amp;what=approve" title="' . $lang_display_comments['approve'] . '">' . cpg_fetch_icon('comment_approve', 0) . '</a>';
                } else {
                    $pending_approval = '<a href="reviewcom.php?pos=-{PID}&amp;msg_id={MSG_ID}&amp;form_token={FORM_TOKEN}&amp;timestamp={TIMESTAMP}&amp;what=disapprove" title="' . $lang_display_comments['disapprove'] . '">' . cpg_fetch_icon('comment_disapprove', 0) . '</a>' . cpg_fetch_icon('comment_approve_disabled', 0);
                }
            } else { // user or guest is logged in - start
                if ($row['approval'] == 'NO') { // the comment is not approved - start
                    if ($user_can_edit) { // the comment comes from the current visitor, display it with a warning that it needs admin approval
                        $pending_approval = cpg_fetch_icon('comment_approval', 0, $lang_display_comments['pending_approval']);
                    } else { // the comment comes from someone else - don't display it at all
                        if ($CONFIG['comment_placeholder'] == 0) {
                            $hide_comment = 1;
                        } else {
                            $row['msg_author'] = $lang_display_comments['unapproved_comment'];
                            $row['msg_body'] = $lang_display_comments['pending_approval_message'];
                            $row['author_id'] = 0;
                        }
                    }
                } // the comment is not approved - end
            } // user or guest is logged in - end

            if ($CONFIG['enable_smilies']) {
                $comment_body = process_smilies(make_clickable($row['msg_body']));
                $smilies = generate_smilies("f{$row['msg_id']}", 'msg_body');
            } else {
                $comment_body = make_clickable($row['msg_body']);
                $smilies = '';
            }

            // wrap the comment into italics if it isn't approved
            if ($row['approval'] == 'NO') {
                $comment_body = '<em>'.$comment_body.'</em>';
                $row['msg_author'] = $row['msg_author'];
            }

            list($row['ip_detail']) = CPGPluginAPI::filter('ip_information', array('', $row['msg_hdr_ip']));
            $ip = $row['msg_hdr_ip']. $row['ip_detail'];
            if ($row['msg_hdr_ip'] != $row['msg_raw_ip']) {
                list($row['ip_detail']) = CPGPluginAPI::filter('ip_information', array('', $row['msg_raw_ip']));
                $ip .= ' [' . $row['msg_raw_ip'] . $row['ip_detail'] . ']';
            }

            list($timestamp, $form_token) = getFormToken();
            $params = array('{EDIT}' => &$comment_edit_box,
                '{BUTTONS}' => &$comment_buttons,
                '{IPINFO}' => &$comment_ipinfo,
                '{PENDING_APPROVAL}' => &$pending_approval,
                '{FORM_TOKEN}' => $form_token,
                '{TIMESTAMP}' => $timestamp,
                );

            $template = template_eval($template_image_comments, $params);

            if ($row['author_id'] == 0) {
                $profile_lnk = stripslashes($row['msg_author']);
            } else {
                $profile_lnk = '<a href="profile.php?uid='.$row['author_id'].'">'.stripslashes($row['msg_author']).'</a>';
            }

            $params = array('{MSG_AUTHOR_LNK}' => $profile_lnk,
                '{MSG_AUTHOR}' => $row['msg_author'],
                '{MSG_ID}' => $row['msg_id'],
                '{PID}' => $row['pid'],
                '{EDIT_TITLE}' => &$lang_display_comments['edit_title'],
                '{DELETE_TITLE}' => &$lang_display_comments['delete_title'],
                '{DELETE_ICON}' => cpg_fetch_icon('delete', 0),
                '{EDIT_ICON}' => cpg_fetch_icon('edit', 0),
                '{CONFIRM_DELETE}' => &$lang_display_comments['confirm_delete'],
                '{MSG_DATE}' => localised_date($row['msg_date'], $lang_date['comment']),
                '{MSG_BODY}' => bb_decode($comment_body),
                '{MSG_BODY_RAW}' => $row['msg_body'],
                '{OK}' => &$lang_common['ok'],
                '{SMILIES}' => $smilies,
                '{IP}' => $ip,
                '{REPORT_COMMENT_TITLE}' => &$lang_display_comments['report_comment_title'],
                '{REPORT_COMMENT_ICON}' => cpg_fetch_icon('report', 0),
                '{WIDTH}' => $CONFIG['picture_table_width'] == "100%" ? $CONFIG['main_table_width'] : $CONFIG['picture_table_width'],
                '{FORM_TOKEN}' => $form_token,
                '{TIMESTAMP}' => $timestamp,
                );

            if ($hide_comment != 1) {
                $html .= template_eval($template, $params);
            }
        } // while-loop end

        $html .= $tabs;
    }
    if (USER_CAN_POST_COMMENTS && $CURRENT_ALBUM_DATA['comments'] == 'YES') {
        if (USER_ID) {
            $user_name_input = '<tr><td colspan="2"><input type="hidden" name="msg_author" value="' . stripslashes(USER_NAME) . '" /></td>';
            template_extract_block($template_add_your_comment, 'user_name_input', $user_name_input);
            $user_name = '';
        } else {
            if (isset($USER['name'])) {
              $user_name = strtr($USER['name'], $HTML_SUBST);
            } else {
              $user_name = $lang_display_comments['your_name'];
            }
        }

        if (($CONFIG['comment_captcha'] == 0) || ($CONFIG['comment_captcha'] == 2 && USER_ID)) {
            template_extract_block($template_add_your_comment, 'comment_captcha');
        } else {
            $template_add_your_comment = CPGPluginAPI::filter('captcha_comment_print', $template_add_your_comment);
        }

        if ($CONFIG['show_bbcode_help']) {
            $captionLabel = '&nbsp;'. cpg_display_help('f=empty.htm&amp;base=64&amp;h='.urlencode(base64_encode(serialize($lang_bbcode_help_title))).'&amp;t='.urlencode(base64_encode(serialize($lang_bbcode_help))),470,245);
        }

        list($timestamp, $form_token) = getFormToken();
        $params = array('{ADD_YOUR_COMMENT}' => $lang_display_comments['add_your_comment'],
            // Modified Name and comment field
            '{NAME}' => $lang_display_comments['name'],
            '{COMMENT}' => $lang_display_comments['comment'],
            '{CONFIRM}' => $lang_common['confirm'].'&nbsp;'. cpg_display_help('f=empty.htm&amp;base=64&amp;h='.urlencode(base64_encode(serialize($lang_common['captcha_help_title']))).'&amp;t='.urlencode(base64_encode(serialize($lang_common['captcha_help']))),470,245),
            '{PIC_ID}' => $pid,
            '{USER_NAME}' => $user_name,
            '{MAX_COM_LENGTH}' => $CONFIG['max_com_size'],
            '{OK}' => $lang_common['ok'],
            '{OK_ICON}' => cpg_fetch_icon('ok', 1),
            '{DEFAULT_USERNAME}' => $lang_display_comments['your_name'],
            '{DEFAULT_USERNAME_MESSAGE}' => $lang_display_comments['default_username_message'],
            '{SMILIES}' => '',
            '{WIDTH}' => $CONFIG['picture_table_width'] == "100%" ? $CONFIG['main_table_width'] : $CONFIG['picture_table_width'],
            '{HELP_ICON}' => $captionLabel,
            '{FORM_TOKEN}' => $form_token,
            '{TIMESTAMP}' => $timestamp,
            );

        if ($CONFIG['enable_smilies']) {
            $params['{SMILIES}'] = generate_smilies();
        } else {
            template_extract_block($template_add_your_comment, 'smilies');
        }

        template_extract_block($template_add_your_comment, 'login_to_comment');
        if ($CONFIG['comments_sort_descending'] == 1) {
            $html = '<br />'.template_eval($template_add_your_comment, $params).$html;
        } else {
            $html .= template_eval($template_add_your_comment, $params);
        }
    } else { // user can not post comments
        if ($CONFIG['comment_promote_registration'] == 1 && $CURRENT_ALBUM_DATA['comments'] == 'YES') {
            template_extract_block($template_add_your_comment, 'user_name_input');
            if ($CONFIG['enable_smilies'] == 1) {
                template_extract_block($template_add_your_comment, 'input_box_smilies');
            } else {
                template_extract_block($template_add_your_comment, 'input_box_no_smilies');
            }
            template_extract_block($template_add_your_comment, 'comment_captcha');
            template_extract_block($template_add_your_comment, 'smilies');
            template_extract_block($template_add_your_comment, 'submit');
            $params = array('{ADD_YOUR_COMMENT}' => $lang_display_comments['add_your_comment'],
                '{WIDTH}' => $CONFIG['picture_table_width'] == "100%" ? $CONFIG['main_table_width'] : $CONFIG['picture_table_width'],
                '{LOGIN_TO_COMMENT}' => sprintf($lang_display_comments['log_in_to_comment'], '<a href="login.php?referer='.$REFERER.'">', '</a>'),
                '{HELP_ICON}' => '',
                );
            if ($CONFIG['comments_sort_descending'] == 1) {
                $html = '<br />'.template_eval($template_add_your_comment, $params).$html;
            } else {
                $html .= template_eval($template_add_your_comment, $params);
            }
        }
    }

    return $html;
}
/******************************************************************************
** Section <<<theme_html_comments>>> - END
******************************************************************************/
