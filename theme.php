<?php
/*************************
  Coppermine Photo Gallery
  ************************
  Copyright (c) 2003-2010 Coppermine Dev Team
  v1.0 originally written by Gregory Demar

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3
  as published by the Free Software Foundation.

  ********************************************
  Coppermine version: 1.5.8
  $HeadURL: https://coppermine.svn.sourceforge.net/svnroot/coppermine/trunk/cpg1.5.x/themes/hardwired/theme.php $
  $Revision: 7805 $
  **********************************************/

// ------------------------------------------------------------------------- //
// This theme has had redundant CORE items removed                           //
// ------------------------------------------------------------------------- //

  define('THEME_HAS_RATING_GRAPHICS',     1);
  define('THEME_HAS_NAVBAR_GRAPHICS',     1);
  define('THEME_HAS_FILM_STRIP_GRAPHIC',  1);
  define('THEME_HAS_NO_SYS_MENU_BUTTONS', 1);
  define('THEME_HAS_NO_SUB_MENU_BUTTONS', 1);
  define('THEME_HAS_PROGRESS_GRAPHICS',   1);



/** function and templates:
* template_breadcrumb
* theme_breadcrumb
* assemble_template_buttons
* addbutton
* template_sys_menu
* template_sub_menu
* template_gallery_admin_menu
* template_img_navbar
* theme_html_img_nav_menu
* template_album_admin_menu
* template_user_admin_menu
* template_tab_display
*/
require_once('theme_navigation.php');

/** function and templates:
* template_cat_list
* template_album_list
* template_thumbnail_view
* theme_display_cat_list
* theme_display_thumbnails
* theme_display_album_list
* template_thumb_view_title_row
*/
require_once('theme_listings.php');

require_once('theme_image.php');

/**  function and templates:
 * template_add_your_comment
 * template_image_comments
 */
require_once('theme_comments.php');





function starttable($width = '-1', $title = '', $title_colspan = '1', $zebra_class = '', $return = false)
{
    global $CONFIG;

    if (GALLERY_ADMIN_MODE) {    //  GALLERY_ADMIN_MODE
        if ($width == '-1') $width = $CONFIG['picture_table_width'];
        if ($width == '100%') $width = $CONFIG['main_table_width'];
        $text = <<<EOT

        <!-- Start standard table --><div class=" row-fluid">
        <table align="center" width="$width" cellspacing="1" cellpadding="0" class="maintable $zebra_class admin_table">

EOT;
    if ($title) {
        $text .= <<<EOT
        <tr>
                <td class="tableh1" colspan="$title_colspan">$title</td>
        </tr>

EOT;
    }
} else {

        if ($width == '-1') $width = $CONFIG['picture_table_width'];
        if ($width == '100%') $width = $CONFIG['main_table_width'];
        $text = <<<EOT

        <div class=" row-fluid">
       

EOT;
        if ($title) {
            $text .= <<<EOT
            <div class="span12">$title</div>
                

EOT;
        } 
}
        if (!$return) {
            echo $text;
        } else {
            return $text;
        }
    
}


/******************************************************************************
** Section <<<endtable>>> - START
******************************************************************************/
function endtable($return = false)
{

    if (GALLERY_ADMIN_MODE) { //GALLERY_ADMIN_MODE

    $text = <<<EOT
    
    </table></div>


EOT;

    } else {


    $text = <<<EOT
    
    </div>


EOT;
 }
    if (!$return) {
        echo $text;
    } else {
        return $text;
    }
}
/******************************************************************************
** Section <<<endtable>>> - END
******************************************************************************/



/******************************************************************************
** Section <<<theme_javascript_head>>> - START
******************************************************************************/
// Function for the JavaScript inside the <head>-section
function theme_javascript_head()
{
    global $JS, $LINEBREAK;

    $return = '';

    // Check if we have any variables being set using set_js_vars function
    if (!empty($JS['vars'])) {
        // Convert the $JS['vars'] array to json object string
        $json_vars = json_encode($JS['vars']);
        // Output the json object
        $return = <<< EOT
<script type="text/javascript">
/* <![CDATA[ */
    var js_vars = $json_vars;
/* ]]> */
</script>

EOT;
    }

    // Check if we have any js includes
    if (!empty($JS['includes'])) {
        // Bring the jquery core library to the very top of the list
        if (in_array('js/jquery-1.3.2.js', $JS['includes']) == TRUE) {
            $key = array_search('js/jquery-1.3.2.js', $JS['includes']);
            unset($JS['includes'][$key]);
            array_unshift($JS['includes'], 'js/jquery-1.3.2.js');
        }
        $JS['includes'] = CPGPluginAPI::filter('javascript_includes',$JS['includes']);
        // Include all the files which were set using js_include() function
        foreach ($JS['includes'] as $js_file) {
            $return .= js_include($js_file, true) . $LINEBREAK;
        }
    }

    return $return;
}
/******************************************************************************
** Section <<<theme_javascript_head>>> - END
******************************************************************************/





/******************************************************************************
** Section <<<pageheader>>> - START
******************************************************************************/
function pageheader($section, $meta = '')
{
    global $CONFIG, $THEME_DIR, $CURRENT_PIC_DATA, $CURRENT_ALBUM_DATA; 
    global $template_header, $lang_charset, $lang_text_dir;

    $custom_header = cpg_get_custom_include($CONFIG['custom_header_path']);

    $charset = ($CONFIG['charset'] == 'language file') ? $lang_charset : $CONFIG['charset'];

    header('P3P: CP="CAO DSP COR CURa ADMa DEVa OUR IND PHY ONL UNI COM NAV INT DEM PRE"');
    header("Content-Type: text/html; charset=$charset");
    user_save_profile();


    //pkk get gal description depending on album and picture
    $descritpion='';
    $title='';

    if(!empty($CURRENT_PIC_DATA['caption'])) $description.=  $CURRENT_PIC_DATA['caption'];
    if(!empty($CURRENT_PIC_DATA['title'])) $title .= '» '.$CURRENT_PIC_DATA['title'] .' - ';

    if(!empty($CURRENT_ALBUM_DATA['title'])) $description.= ' '.$CURRENT_ALBUM_DATA['title'];
    if(!empty($CURRENT_ALBUM_DATA['description'])) $description.= ' '.$CURRENT_ALBUM_DATA['description'];

    $tmp = str_replace(' ','',$description);
    if(empty($tmp)) {
        $description = $CONFIG['gallery_name'] . ' - ' . $CONFIG['gallery_description'];
    }

    $template_vars = array(
        '{LANG_DIR}' => $lang_text_dir,
        '{TITLE}' => $title.theme_page_title($section),
        '{CHARSET}' => $charset,
        '{META}' => $meta,
        '{GAL_NAME}' => $CONFIG['gallery_name'],
        '{GAL_DESCRIPTION}' => $description,
        '{SYS_MENU}' => theme_main_menu('sys_menu'),
        '{SUB_MENU}' => theme_main_menu('sub_menu'),
        '{ADMIN_MENU}' => theme_admin_mode_menu(),
        '{CUSTOM_HEADER}' => $custom_header,
        '{JAVASCRIPT}' => theme_javascript_head(),
        '{MESSAGE_BLOCK}' => theme_display_message_block(),
    );
    
    $template_vars = CPGPluginAPI::filter('theme_pageheader_params', $template_vars);
    echo template_eval($template_header, $template_vars);

    // Show various admin messages
    adminmessages();
}
/******************************************************************************
** Section <<<pageheader>>> - END
******************************************************************************/


/******************************************************************************
** Section <<<theme_page_title>>> - START
******************************************************************************/
// Creates the title tag for each page
// For the sake of search engine friendliness, the dynamic part $section should come first
function theme_page_title($section) 
{
    global $CONFIG;
    $return = strip_tags(bb_decode($section)) . ' - ' . $CONFIG['gallery_name'];
    return $return;
}
/******************************************************************************
** Section <<<theme_page_title>>> - END
******************************************************************************/




?>
