Blankstrap Theme for Coppermine v0.2
===============================

License
-------------
WTFPL [link](http://en.wikipedia.org/wiki/WTFPL)

Description
-----------

Simple Coppermine theme build based on Twitters Bootstrap. It can be easly themed using a custom bootstrap theme. You can download themes from [bootswatch](http://http://bootswatch.com/) or create Your own [here](http://twitter.github.io/bootstrap/customize.html)

[Coppermine forum](http://forum.coppermine-gallery.net/index.php/topic,76309.0.html)

Demo
-----
[See demo](http://pawelkoston.pl/demos/coppermine/)

Download
--------
- [Zip](https://bitbucket.org/pawelkoston/blankstrap-coppermine-theme/)
- or `git clone https://bitbucket.org/pawelkoston/blankstrap-coppermine-theme.git blankstrap`

Known Issues
------------
- The admin inrtface is still based on &lt;table&gt;'s and it needs some improvemnts
- You still get old jQuery 1.3
- Some of the theme elements (mostly not used by default) are not included



Changelog
-------------
+ v0.2 
   - Little visual improvement
   - Included Icons by Glyphicons
   - Changed thumbanil view, so that whole thumbnails are visible
+ v0.1 
