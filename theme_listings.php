<?php

/******************************************************************************
** Section <<<$template_cat_list>>> - START
******************************************************************************/
// HTML template for the category list
$template_cat_list = <<<EOT
<!-- BEGIN header -->
  <ul class="thumbnails">
<!-- END header -->
<!-- BEGIN catrow_noalb -->
        <li class="span3 cat-list">
            {CAT_THUMB}
            <div class="thumbnail ">
                <div class="caption">
                    <h3><span class="catlink">{CAT_TITLE}</span></h3>
                    <p>
                        {CAT_DESC}
                    </p>

                </div>
            </div>
        </li>
<!-- END catrow_noalb -->
<!-- BEGIN catrow -->
        <li class="span3  cat-thumb">
            <div class="thumbnail">
                {CAT_THUMB}
                <div class="caption">
                    <h3><span class="catlink">{CAT_TITLE}</span></h3>
                    <p >
                        {CAT_DESC}
                       
                    </p>
                    
                    <p>
                        <strong>Alubmy: </strong>{ALB_COUNT} <br/>
                        <strong>Zdjęcia: </strong>{PIC_COUNT}
                        <a href="{CAT_LNK}" class="btn btn-primary  pull-right cl">Przeglądaj</a>
                    </p>
                    
                </div>
            </div>
        </li>
     
<!-- END catrow -->
<!-- BEGIN footer -->
        <li class="span12">
               <p class="text-right muted"> {STATISTICS} </p>
        </li>
        </ul>
<!-- END footer -->
<!-- BEGIN spacer -->
        
<!-- END spacer -->

EOT;
/******************************************************************************
** Section <<<$template_cat_list>>> - END
******************************************************************************/





/******************************************************************************
** Section <<<$template_album_list>>> - START
******************************************************************************/
// HTML template for the album list
$template_album_list = <<<EOT

<!-- BEGIN stat_row -->

<!-- END stat_row -->
<!-- BEGIN header -->
      <ul class="thumbnails">
<!-- END header -->
<!-- BEGIN album_cell -->
        <li class="span3 album-list">
            <div class="thumbnail ">
                <a href="{ALB_LINK_TGT}" class="albums">{ALB_LINK_PIC}</a>
                <div class="caption">
                    <h3><span class="catlink"><a title="{ALBUM_TITLE}" href="{ALB_LINK_TGT}">{ALBUM_TITLE}</a></span></h3>
                    <p class="album-description">{ALB_DESC}</p>
                    
                     
                     <a href="{ALB_LINK_TGT}" class="btn btn-primary pull-right cl">Przeglądaj</a>
                     <p>
                        {ADMIN_MENU}
                     </p>
                     <div class="clearfix"></div>
                </div>
            </div>
        </li> 
        
<!-- END album_cell -->
<!-- BEGIN empty_cell -->
        
<!-- END empty_cell -->
<!-- BEGIN row_separator -->
        
<!-- END row_separator -->
<!-- BEGIN footer -->
      </ul>
<!-- END footer -->
<!-- BEGIN tabs -->
     <div class="span12">
        {TABS}
     </div>
<!-- END tabs -->
<!-- BEGIN spacer -->
       
<!-- END spacer -->

EOT;
/******************************************************************************
** Section <<<$template_album_list>>> - END
******************************************************************************/



/******************************************************************************
** Section <<<$template_thumbnail_view>>> - START
******************************************************************************/

// HTML template for thumbnails display
$template_thumbnail_view = <<<EOT

<!-- BEGIN header -->
        <div class="span12"><ul class="thumbnails">
<!-- END header -->
<!-- BEGIN thumb_cell -->
        
            <li class="span3 thumbnail-view">
                <div class="thumbnail album-thumb">
                    <a href="{LINK_TGT}">{THUMB}</a>
                    <div class="caption">
                        
                        {CAPTION}

                    </div>
                    {ADMIN_MENU}
                </div>
            </li>

<!-- END thumb_cell -->
<!-- BEGIN empty_cell -->
                
<!-- END empty_cell -->
<!-- BEGIN row_separator -->
        
<!-- END row_separator -->
<!-- BEGIN footer -->
        </ul></div>
<!-- END footer -->
<!-- BEGIN tabs -->
<div class="span12">
       {TABS}
</div>
<!-- END tabs -->
<!-- BEGIN spacer -->
      
<!-- END spacer -->

EOT;
/******************************************************************************
** Section <<<$template_thumbnail_view>>> - END
******************************************************************************/



/******************************************************************************
** Section <<<theme_display_cat_list>>> - START
******************************************************************************/
function theme_display_cat_list($breadcrumb, &$cat_data, $statistics)
{
    global $template_cat_list, $lang_cat_list;
    if (count($cat_data) > 0) {
        starttable('100%');
        $template = template_extract_block($template_cat_list, 'header');
        $params = array(
                '{CATEGORY}' => $lang_cat_list['category'],
                '{ALBUMS}' => $lang_cat_list['albums'],
                '{PICTURES}' => $lang_cat_list['pictures'],
        );
        echo template_eval($template, $params);
    }

    $template_noalb = template_extract_block($template_cat_list, 'catrow_noalb');
    $template = template_extract_block($template_cat_list, 'catrow');
    foreach($cat_data as $category) {

	try {
            $el = &simplexml_load_string('<tmp>'.$category['cat_thumb'].'</tmp>');
            $attribs = &$el->a->img->attributes();
            unset($attribs->title);
            unset($attribs->border);
            $category['cat_thumb'] = $el->a->asXML();
        } catch (Exception $e)  {

        }
        preg_match('/<a href=\"(.*?)\">/', $category[0], $matches);  
        if (!isset($category['cat_thumb'])) { $category['cat_thumb'] = ''; }
        if (count($category) == 3) {
            $params = array(
                    '{CAT_TITLE}' => $category[0],
                    '{CAT_THUMB}' => $category['cat_thumb'],
                    '{CAT_DESC}' => $category[1],
                    '{CAT_LNK}' =>$matches[1],
                 
            );
            echo template_eval($template_noalb, $params);
        } elseif (isset($category['cat_albums']) && ($category['cat_albums'] != '')) {
            $params = array(
                    '{CAT_TITLE}' => $category[0],
                    '{CAT_THUMB}' => $category['cat_thumb'],
                    '{CAT_DESC}' => $category[1],
                    '{CAT_ALBUMS}' => $category['cat_albums'],
                    '{ALB_COUNT}' => cpg_float2decimal($category[2]),
                    '{PIC_COUNT}' => cpg_float2decimal($category[3]),
                    '{CAT_LNK}' =>$matches[1],
            );
            echo template_eval($template, $params);
        } else {
            $params = array(
                    '{CAT_TITLE}' => $category[0],
                    '{CAT_THUMB}' => $category['cat_thumb'],
                    '{CAT_DESC}' => $category[1],
                    '{CAT_ALBUMS}' => '',
                    '{ALB_COUNT}' => cpg_float2decimal($category[2]),
                    '{PIC_COUNT}' => cpg_float2decimal($category[3]),
                    '{CAT_LNK}' =>$matches[1],
            );
            echo template_eval($template, $params);
        }
    }

    if ($statistics && count($cat_data) > 0) {
        $template = template_extract_block($template_cat_list, 'footer');
        $params = array('{STATISTICS}' => $statistics);
        echo template_eval($template, $params);
    }


    if (count($cat_data) > 0)
          endtable();
        echo template_extract_block($template_cat_list, 'spacer');
}
/******************************************************************************
** Section <<<theme_display_cat_list>>> - END
******************************************************************************/



/******************************************************************************
** Section <<<theme_display_thumbnails>>> - START
******************************************************************************/
function theme_display_thumbnails(&$thumb_list, $nbThumb, $album_name, $aid, $cat, $page, $total_pages, $sort_options, $display_tabs, $mode = 'thumb', $date='')
{
    global $CONFIG, $CURRENT_ALBUM_DATA;
    global $template_thumb_view_title_row,$template_fav_thumb_view_title_row, $lang_thumb_view, $lang_common, $template_tab_display, $template_thumbnail_view, $lang_album_list, $lang_errors;

    $superCage = Inspekt::makeSuperCage();

    static $header        = '';
    static $thumb_cell    = '';
    static $empty_cell    = '';
    static $row_separator = '';
    static $footer        = '';
    static $tabs          = '';
    static $spacer        = '';

    if ($header == '') {
        $thumb_cell    = template_extract_block($template_thumbnail_view, 'thumb_cell');
        $tabs          = template_extract_block($template_thumbnail_view, 'tabs');
        $header        = template_extract_block($template_thumbnail_view, 'header');
        $empty_cell    = template_extract_block($template_thumbnail_view, 'empty_cell');
        $row_separator = template_extract_block($template_thumbnail_view, 'row_separator');
        $footer        = template_extract_block($template_thumbnail_view, 'footer');
        $spacer        = template_extract_block($template_thumbnail_view, 'spacer');
    }

    $cat_link  = is_numeric($aid) ? '' : '&amp;cat=' . $cat;
    $date_link = $date=='' ? '' : '&amp;date=' . $date;
    if ($superCage->get->getInt('uid')) {
      $uid_link = '&amp;uid=' . $superCage->get->getInt('uid');
    } else {
      $uid_link = '';
    }

    $album_types = array(
        'albums' => array('lastalb')
    );
    $album_types = CPGPluginAPI::filter('theme_thumbnails_album_types', $album_types);

    $theme_thumb_tab_tmpl = $template_tab_display;

    if ($mode == 'thumb') {
        $theme_thumb_tab_tmpl['left_text'] = strtr($theme_thumb_tab_tmpl['left_text'], array('{LEFT_TEXT}' => in_array($aid, $album_types['albums']) ? $lang_album_list['album_on_page'] : $lang_thumb_view['pic_on_page']));
        $theme_thumb_tab_tmpl['page_link'] = strtr($theme_thumb_tab_tmpl['page_link'], array('{LINK}' => 'thumbnails.php?album=' . $aid . $cat_link . $date_link . $uid_link . '&amp;page=%d'));
    } else {
        $theme_thumb_tab_tmpl['left_text'] = strtr($theme_thumb_tab_tmpl['left_text'], array('{LEFT_TEXT}' => $lang_thumb_view['user_on_page']));
        $theme_thumb_tab_tmpl['page_link'] = strtr($theme_thumb_tab_tmpl['page_link'], array('{LINK}' => 'index.php?cat=' . $cat . '&amp;page=%d'));
    }

    $thumbcols = $CONFIG['thumbcols'];
    $cell_width = ceil(100 / $CONFIG['thumbcols']) . '%';

    $tabs_html = $display_tabs ? create_tabs($nbThumb, $page, $total_pages, $theme_thumb_tab_tmpl) : '';

    if (!GALLERY_ADMIN_MODE && stripos($template_thumb_view_title_row, 'admin_buttons') !== false) {
        template_extract_block($template_thumb_view_title_row, 'admin_buttons');
    }
    // The sort order options are not available for meta albums
    if ($sort_options) {
        if (GALLERY_ADMIN_MODE) {
            $param = array(
                '{ALBUM_ID}'       => $aid,
                '{CAT_ID}'         => ($cat > 0 ? $cat : $CURRENT_ALBUM_DATA['category']),
                '{MODIFY_LNK}'     => $lang_common['album_properties'],
                '{MODIFY_ICO}'     => cpg_fetch_icon('modifyalb', 1),
                '{PARENT_CAT_LNK}' => $lang_common['parent_category'],
                '{PARENT_CAT_ICO}' => cpg_fetch_icon('category', 1),
                '{EDIT_PICS_LNK}'  => $lang_common['edit_files'],
                '{EDIT_PICS_ICO}'  => cpg_fetch_icon('edit', 1),
                '{ALBUM_MGR_LNK}'  => $lang_common['album_manager'],
                '{ALBUM_MGR_ICO}'  => cpg_fetch_icon('alb_mgr', 1),
            );
        } else {
            $param = array();
        }
        $param['{ALBUM_NAME}'] = $album_name;
        // Plugin Filter: allow plugin to modify or add tags to process
        $param = CPGPluginAPI::filter('theme_thumbnails_title', $param);
        $title = template_eval($template_thumb_view_title_row, $param);
    } elseif ($aid == 'favpics' && $CONFIG['enable_zipdownload'] > 0) { //Lots of stuff can be added here later
        $param = array(
            '{ALBUM_ID}'   => $aid,
            '{ALBUM_NAME}' => $album_name,
            '{DOWNLOAD_ZIP}' => cpg_fetch_icon ('zip', 2) . $lang_thumb_view['download_zip'],
        );
        // Plugin Filter: allow plugin to modify or add tags to process
        $param = CPGPluginAPI::filter('theme_thumbnails_title', $param);        
        $title = template_eval($template_fav_thumb_view_title_row, $param);
    } else {
        $title = $album_name;
    }

    CPGPluginAPI::action('theme_thumbnails_wrapper_start', null);

    if ($mode == 'thumb') {
        starttable('100%', $title, $thumbcols);
    } else {
        starttable('100%');
    }

    echo $header;



    $i = 0;
    global $thumb;  // make $thumb accessible to plugins
    foreach($thumb_list as $thumb) {
        $i++;
        
        try {
            $el = &simplexml_load_string('<tmp>'.$thumb['image'].'</tmp>');
            $attribs = &$el->img->attributes();
            unset($attribs->title);
            unset($attribs->border);
            $thumb['image'] = $el->img->asXML();
        } catch (Exception $e)  {

        }
        if ($mode == 'thumb') { 
            if (in_array($aid, $album_types['albums'])) {
                $params = array(
                    '{CELL_WIDTH}' => $cell_width,
                    '{LINK_TGT}'   => "thumbnails.php?album={$thumb['aid']}",
                    '{THUMB}'      => $thumb['image'],
                    '{CAPTION}'    => str_replace('<br />','',$thumb['caption']),
                    '{ADMIN_MENU}' => $thumb['admin_menu'],
                );
            } else {
                // determine if thumbnail link targets should open in a pop-up
                if ($CONFIG['thumbnail_to_fullsize'] == 1) { // code for full-size pop-up
                    if (!USER_ID && $CONFIG['allow_unlogged_access'] <= 2) {
                       $target = 'javascript:;" onclick="alert(\''.sprintf($lang_errors['login_needed'],'','','','').'\');';
                    } elseif (USER_ID && USER_ACCESS_LEVEL <= 2) {
                        $target = 'javascript:;" onclick="alert(\''.sprintf($lang_errors['access_intermediate_only'],'','','','').'\');';
                    } else {
                       $target = 'javascript:;" onclick="MM_openBrWindow(\'displayimage.php?pid=' . $thumb['pid'] . '&fullsize=1\',\'' . uniqid(rand()) . '\',\'scrollbars=yes,toolbar=no,status=no,resizable=yes,width=' . ((int)$thumb['pwidth']+(int)$CONFIG['fullsize_padding_x']) .  ',height=' .   ((int)$thumb['pheight']+(int)$CONFIG['fullsize_padding_y']). '\');';
                    }
                } elseif ($aid == 'random') {
                    $target = "displayimage.php?pid={$thumb['pid']}$uid_link#top_display_media";
                } elseif ($aid == 'lastcom' || $aid == 'lastcomby') {
                    $page = cpg_get_comment_page_number($thumb['msg_id']);
                    $page = (is_numeric($page)) ? "&amp;page=$page" : '';
                    $target = "displayimage.php?album=$aid$cat_link$date_link&amp;pid={$thumb['pid']}$uid_link&amp;msg_id={$thumb['msg_id']}$page#comment{$thumb['msg_id']}";
                } else {
                    $target = "displayimage.php?album=$aid$cat_link$date_link&amp;pid={$thumb['pid']}$uid_link#top_display_media";
                }
                $params = array(
                    '{CELL_WIDTH}' => $cell_width,
                    '{LINK_TGT}'   => $target,
                    '{THUMB}'      => $thumb['image'],
                    '{CAPTION}'    =>  str_replace('<br />','',$thumb['caption']),
                    '{ADMIN_MENU}' => $thumb['admin_menu'],
                );
            }

        } else {  // mode != 'thumb'

            // Used for mode = 'user' from list_users() in index.php
            $params = array(
                '{CELL_WIDTH}' => $cell_width,
                '{LINK_TGT}'   => "index.php?cat={$thumb['cat']}",
                '{THUMB}'      => $thumb['image'],
                '{CAPTION}'    => str_replace('<br />','',$thumb['caption']),
                '{ADMIN_MENU}' => '',
            );

        }

        // Plugin Filter: allow plugin to modify or add tags to process
        $params = CPGPluginAPI::filter('theme_display_thumbnails_params', $params);
        echo template_eval($thumb_cell, $params);

        if ((($i % $thumbcols) == 0) && ($i < count($thumb_list))) {
            echo $row_separator;
        }
    } // foreach $thumb

    unset($thumb);  // unset $thumb to avoid conflicting with global

    for (;($i % $thumbcols); $i++) {
        echo $empty_cell;
    }
    $footer = CPGPluginAPI::filter('theme_thumbnails_footer', $footer); 
    echo $footer;

    if ($display_tabs) {
        $params = array(
            '{THUMB_COLS}' => $thumbcols,
            '{TABS}'       => $tabs_html,
        );
        echo template_eval($tabs, $params);
    }

    endtable();
    CPGPluginAPI::action('theme_thumbnails_wrapper_end', null);
    echo $spacer;
}
/******************************************************************************
** Section <<<theme_display_thumbnails>>> - END
******************************************************************************/


/******************************************************************************
** Section <<<theme_display_album_list>>> - START
******************************************************************************/
function theme_display_album_list(&$alb_list, $nbAlb, $cat, $page, $total_pages)
{

    global $CONFIG, $STATS_IN_ALB_LIST, $statistics, $template_tab_display, $template_album_list, $lang_album_list;

    $theme_alb_list_tab_tmpl = $template_tab_display;

    $theme_alb_list_tab_tmpl['left_text'] = strtr($theme_alb_list_tab_tmpl['left_text'], array('{LEFT_TEXT}' => $lang_album_list['album_on_page']));
    $theme_alb_list_tab_tmpl['page_link'] = strtr($theme_alb_list_tab_tmpl['page_link'], array('{LINK}' => 'index.php?cat=' . $cat . '&amp;page=%d'));

    $tabs = create_tabs($nbAlb, $page, $total_pages, $theme_alb_list_tab_tmpl);

    $album_cell = template_extract_block($template_album_list, 'album_cell');
    $empty_cell = template_extract_block($template_album_list, 'empty_cell');
    $tabs_row = template_extract_block($template_album_list, 'tabs');
    $stat_row = template_extract_block($template_album_list, 'stat_row');
    $spacer = template_extract_block($template_album_list, 'spacer');
    $header = template_extract_block($template_album_list, 'header');
    $footer = template_extract_block($template_album_list, 'footer');
    $rows_separator = template_extract_block($template_album_list, 'row_separator');

    $count = 0;

    $columns = $CONFIG['album_list_cols'];
    $column_width = ceil(100 / $columns);
    $thumb_cell_width = $CONFIG['alb_list_thumb_size'] + 2;

    starttable('100%');

    if ($STATS_IN_ALB_LIST) {
        $params = array('{STATISTICS}' => $statistics,
            '{COLUMNS}' => $columns,
            );
        echo template_eval($stat_row, $params);
    }

    echo $header;

    if (is_array($alb_list)) {
        foreach($alb_list as $album) {
            $count ++;
            $album['album_desc'] = str_replace(array('<br />','<br/>','<br>',"\n"),'',$album['album_desc']);
            $params = array('{COL_WIDTH}' => $column_width,
                '{ALBUM_TITLE}' => $album['album_title'],
                '{THUMB_CELL_WIDTH}' => $thumb_cell_width,
                '{ALB_LINK_TGT}' => "thumbnails.php?album={$album['aid']}",
                '{ALB_LINK_PIC}' => $album['thumb_pic'],
                '{ADMIN_MENU}' => $album['album_adm_menu'],
                '{ALB_DESC}' => ($album['album_desc']) ? $album['album_desc'] : $album['album_info'],
                '{ALB_INFOS}' => $album['album_info'],
                '{ALB_HITS}' => $album['alb_hits'],
                );

            echo template_eval($album_cell, $params);

            if ($count % $columns == 0 && $count < count($alb_list)) {
                echo $rows_separator;
            }
        }
    }

    $params = array('{COL_WIDTH}' => $column_width,
          '{SPACER}' => $thumb_cell_width
          );
    $empty_cell = template_eval($empty_cell, $params);

    while ($count++ % $columns != 0) {
        echo $empty_cell;
    }

    echo $footer;
    // Tab display
    $params = array('{COLUMNS}' => $columns,
        '{TABS}' => $tabs,
        );
    echo template_eval($tabs_row, $params);

    endtable();

    echo $spacer;
}
/******************************************************************************
** Section <<<theme_display_album_list>>> - END
******************************************************************************/




/******************************************************************************
** Section <<<$template_thumb_view_title_row>>> - START
******************************************************************************/
// HTML template for title row of the thumbnail view (album title + sort options)
$template_thumb_view_title_row = <<<EOT

                    <h1>{ALBUM_NAME}</h1>

<!-- BEGIN admin_buttons -->
                <div class="btn-group pull-right span3">
                    <a href="modifyalb.php?album={ALBUM_ID}" class="admin_menu btn" title="{MODIFY_LNK}">{MODIFY_ICO}</a>
                    &nbsp;&nbsp;
                    <a href="index.php?cat={CAT_ID}" class="admin_menu btn" title="{PARENT_CAT_LNK}">{PARENT_CAT_ICO}</a>
                    &nbsp;&nbsp;
                    <a href="editpics.php?album={ALBUM_ID}" class="admin_menu btn" title="{EDIT_PICS_LNK}">{EDIT_PICS_ICO}</a>
                    &nbsp;&nbsp;
                    <a href="albmgr.php?cat={CAT_ID}" class="admin_menu btn" title="{ALBUM_MGR_LNK}">{ALBUM_MGR_ICO}</a>
                </div>
<!-- END admin_buttons -->
                
                <div style="text-align:left;" class="sortorder_cell span9" id="sortorder_cell">
                    <!-- Use JavaScript to display the sorting options only to humans, but hide them from search engines to avoid double-content indexing (js/thumbnails.js) -->
                </div>
EOT;
/******************************************************************************
** Section <<<$template_thumb_view_title_row>>> - END
******************************************************************************/


