<?php





/******************************************************************************
** Section <<<$template_breadcrumb>>> - START
******************************************************************************/
// HTML template for the breadcrumb
$template_breadcrumb = <<<EOT
<!-- BEGIN breadcrumb -->
        <ul class="breadcrumb ">
          <li> <span class="statlink">{BREADCRUMB}</span></li>
          
        </ul>
      
<!-- END breadcrumb -->
<!-- BEGIN breadcrumb_user_gal -->
        <ul class="breadcrumb ">
          <li> <span class="statlink"><span class="statlink">{BREADCRUMB}</span>, <span class="statlink">::{STATISTICS}</span></span></li>
          
        </ul>

<!-- END breadcrumb_user_gal -->

EOT;



/******************************************************************************
** Section <<<$template_breadcrumb>>> - END
******************************************************************************/
/******************************************************************************
** Section <<<theme_breadcrumb>>> - START
******************************************************************************/
// Function for building the breadcrumb
// Inputs:  $breadcrumb_links, $BREADCRUMB_TEXTS
// Outputs: $breadcrumb, $BREADCRUMB_TEXT
function theme_breadcrumb($breadcrumb_links, $BREADCRUMB_TEXTS, &$breadcrumb, &$BREADCRUMB_TEXT)
{
    $breadcrumb = '';
    $BREADCRUMB_TEXT = '';
    foreach ($breadcrumb_links as $breadcrumb_link) {
      
        $breadcrumb .= ' > ' . $breadcrumb_link;
    }
    foreach ($BREADCRUMB_TEXTS as $BREADCRUMB_TEXT_elt) {
        $BREADCRUMB_TEXT .= ' > ' . $BREADCRUMB_TEXT_elt;
    }
    // We remove the first ' > '
    $breadcrumb = substr_replace($breadcrumb,'', 0, 3);
    $BREADCRUMB_TEXT = substr_replace($BREADCRUMB_TEXT,'', 0, 3);
}
/******************************************************************************
** Section <<<theme_breadcrumb>>> - END
******************************************************************************/




/******************************************************************************
** Section <<<assemble_template_buttons>>> - START
******************************************************************************/
// Creates buttons from a template using an array of tokens
// this function is used in this file it needs to be declared before being called.
function assemble_template_buttons($template_buttons,$buttons) 
{
    $counter=0;
    $output='';

    foreach ($buttons as $button)  {
        if (isset($button[4])) {
            $spacer=$button[4];
        } else {
            $spacer='';
        }

        $params = array(
            '{SPACER}'     => $spacer,
            '{BLOCK_ID}'   => $button[3],
            '{HREF_TGT}'   => $button[2],
            '{HREF_TITLE}' => $button[1],
            '{HREF_LNK}'   => $button[0],
            '{HREF_ATTRIBUTES}'   => $button[5]
            );
        $output.=template_eval($template_buttons, $params);
    }
    return $output;
}
/******************************************************************************
** Section <<<assemble_template_buttons>>> - END
******************************************************************************/




/******************************************************************************
** Section <<<addbutton>>> - START
******************************************************************************/
// Creates an array of tokens to be used with function assemble_template_buttons
// this function is used in this file it needs to be declared before being called.
function addbutton(&$menu,$href_lnk,$href_title,$href_tgt,$block_id,$spacer,$href_attrib='') 
{
    $menu[]=array($href_lnk,$href_title,$href_tgt,$block_id,$spacer,$href_attrib);
}
/******************************************************************************
** Section <<<addbutton>>> - END
******************************************************************************/




/******************************************************************************
** Section <<<$template_sys_menu>>> - START
******************************************************************************/
// HTML template for sys_menu
$template_sys_menu = <<<EOT
          {BUTTONS}
EOT;
/******************************************************************************
** Section <<<$template_sys_menu>>> - END
******************************************************************************/



/******************************************************************************
** Section <<<$template_sub_menu>>> - START
******************************************************************************/
// HTML template for sub_menu
$template_sub_menu = $template_sys_menu;


if (!defined('THEME_HAS_NO_SYS_MENU_BUTTONS')) {

  // HTML template for template sys_menu spacer

  $template_sys_menu_spacer = '<img src="themes/water_drop/images/orange_carret.gif" width="8" height="8"  alt="" />';

  // HTML template for template sys_menu buttons

  $template_sys_menu_button = <<<EOT
  <!-- BEGIN {BLOCK_ID} -->
        <a href="{HREF_TGT}" title="{HREF_TITLE}" {HREF_ATTRIBUTES}>{HREF_LNK}</a> {SPACER}
  <!-- END {BLOCK_ID} -->
EOT;

  // HTML template for template sys_menu buttons

    // {HREF_LNK}{HREF_TITLE}{HREF_TGT}{BLOCK_ID}{SPACER}{HREF_ATTRIBUTES}
    addbutton($sys_menu_buttons,'{HOME_LNK}','{HOME_TITLE}','{HOME_TGT}','home',$template_sys_menu_spacer);
    addbutton($sys_menu_buttons,'{CONTACT_LNK}','{CONTACT_TITLE}','{CONTACT_TGT}','contact',$template_sys_menu_spacer);
    addbutton($sys_menu_buttons,'{MY_GAL_LNK}','{MY_GAL_TITLE}','{MY_GAL_TGT}','my_gallery',$template_sys_menu_spacer);
    addbutton($sys_menu_buttons,'{MEMBERLIST_LNK}','{MEMBERLIST_TITLE}','{MEMBERLIST_TGT}','allow_memberlist',$template_sys_menu_spacer);
    if (array_key_exists('allowed_albums', $USER_DATA) && is_array($USER_DATA['allowed_albums']) && count($USER_DATA['allowed_albums'])) {
      addbutton($sys_menu_buttons,'{UPL_APP_LNK}','{UPL_APP_TITLE}','{UPL_APP_TGT}','upload_approval',$template_sys_menu_spacer);
    }
    addbutton($sys_menu_buttons,'{MY_PROF_LNK}','{MY_PROF_TITLE}','{MY_PROF_TGT}','my_profile',$template_sys_menu_spacer);
    addbutton($sys_menu_buttons,'{ADM_MODE_LNK}','{ADM_MODE_TITLE}','{ADM_MODE_TGT}','enter_admin_mode',$template_sys_menu_spacer);
    addbutton($sys_menu_buttons,'{USR_MODE_LNK}','{USR_MODE_TITLE}','{USR_MODE_TGT}','leave_admin_mode',$template_sys_menu_spacer);
    addbutton($sys_menu_buttons,'{SIDEBAR_LNK}','{SIDEBAR_TITLE}','{SIDEBAR_TGT}','sidebar',$template_sys_menu_spacer);
    addbutton($sys_menu_buttons,'{UPL_PIC_LNK}','{UPL_PIC_TITLE}','{UPL_PIC_TGT}','upload_pic',$template_sys_menu_spacer);
    addbutton($sys_menu_buttons,'{REGISTER_LNK}','{REGISTER_TITLE}','{REGISTER_TGT}','register',$template_sys_menu_spacer);
    addbutton($sys_menu_buttons,'{LOGIN_LNK}','{LOGIN_TITLE}','{LOGIN_TGT}','login','');
    addbutton($sys_menu_buttons,'{LOGOUT_LNK}','{LOGOUT_TITLE}','{LOGOUT_TGT}','logout','');
    // Login and Logout don't have a spacer as only one is shown, and either would be the last option.



  $sys_menu_buttons = CPGPluginAPI::filter('sys_menu',$sys_menu_buttons);
  $params = array('{BUTTONS}' => assemble_template_buttons($template_sys_menu_button,$sys_menu_buttons));
  $template_sys_menu = template_eval($template_sys_menu,$params);
}
/******************************************************************************
** Section <<<$template_sub_menu>>> - END
******************************************************************************/

/******************************************************************************
** Section <<<THEME_HAS_NO_SUB_MENU_BUTTONS>>> - START
******************************************************************************/
if (!defined('THEME_HAS_NO_SUB_MENU_BUTTONS')) {

    // HTML template for template sub_menu spacer

    $template_sub_menu_spacer = $template_sys_menu_spacer;

    // HTML template for template sub_menu buttons

    $template_sub_menu_button = $template_sys_menu_button;

    // HTML template for template sub_menu buttons

    // {HREF_LNK}{HREF_TITLE}{HREF_TGT}{BLOCK_ID}{SPACER}{HREF_ATTRIBUTES}
    addbutton($sub_menu_buttons,'{CUSTOM_LNK_LNK}','{CUSTOM_LNK_TITLE}','{CUSTOM_LNK_TGT}','custom_link',$template_sub_menu_spacer);
    addbutton($sub_menu_buttons,'{ALB_LIST_LNK}','{ALB_LIST_TITLE}','{ALB_LIST_TGT}','album_list',$template_sub_menu_spacer);
    addbutton($sub_menu_buttons,'{LASTUP_LNK}','{LASTUP_TITLE}','{LASTUP_TGT}','lastup',$template_sub_menu_spacer,'rel="nofollow"');
    addbutton($sub_menu_buttons,'{LASTCOM_LNK}','{LASTCOM_TITLE}','{LASTCOM_TGT}','lastcom',$template_sub_menu_spacer,'rel="nofollow"');
    addbutton($sub_menu_buttons,'{TOPN_LNK}','{TOPN_TITLE}','{TOPN_TGT}','topn',$template_sub_menu_spacer,'rel="nofollow"');
    addbutton($sub_menu_buttons,'{TOPRATED_LNK}','{TOPRATED_TITLE}','{TOPRATED_TGT}','toprated',$template_sub_menu_spacer,'rel="nofollow"');
    addbutton($sub_menu_buttons,'{FAV_LNK}','{FAV_TITLE}','{FAV_TGT}','favpics',$template_sub_menu_spacer,'rel="nofollow"');
    if ($CONFIG['browse_by_date'] != 0) {
        addbutton($sub_menu_buttons, '{BROWSEBYDATE_LNK}', '{BROWSEBYDATE_TITLE}', '{BROWSEBYDATE_TGT}', 'browse_by_date', $template_sub_menu_spacer, 'rel="nofollow" class="greybox"');
    }
    addbutton($sub_menu_buttons,'{SEARCH_LNK}','{SEARCH_TITLE}','{SEARCH_TGT}','search','');


    $sub_menu_buttons = CPGPluginAPI::filter('sub_menu',$sub_menu_buttons);
    $params = array('{BUTTONS}' => assemble_template_buttons($template_sub_menu_button,$sub_menu_buttons));
    $template_sub_menu = template_eval($template_sub_menu,$params);
}
/******************************************************************************
** Section <<<THEME_HAS_NO_SUB_MENU_BUTTONS>>> - END
******************************************************************************/

  



// HTML template for sys menu
  $template_sys_menu = <<< EOT
  <ul class="nav">
  <!-- BEGIN home -->


  <li>
  <a href="{HOME_TGT}" title="{HOME_TITLE}">{HOME_LNK}</a>
  </li>

  <!-- END home -->
  <!-- BEGIN contact -->


  <li>
  <a href="{CONTACT_TGT}" title="{CONTACT_TITLE}">{CONTACT_LNK}</a>
  </li>

  <!-- END contact -->
  <!-- BEGIN my_gallery -->


  <li>
  <a href="{MY_GAL_TGT}" title="{MY_GAL_TITLE}">{MY_GAL_LNK}</a>
  </li>

  <!-- END my_gallery -->
  <!-- BEGIN allow_memberlist -->


  <li>
  <a href="{MEMBERLIST_TGT}" title="{MEMBERLIST_TITLE}">{MEMBERLIST_LNK}</a>
  </li>

  <!-- END allow_memberlist -->
  <!-- BEGIN my_profile -->


  <li>
  <a href="{MY_PROF_TGT}" title="{MY_PROF_LNK}">{MY_PROF_LNK}</a>
  </li>

  <!-- END my_profile -->
  <!-- BEGIN enter_admin_mode -->


  <li>
  <a href="{ADM_MODE_TGT}" title="{ADM_MODE_TITLE}">{ADM_MODE_LNK}</a>
  </li>

  <!-- END enter_admin_mode -->
  <!-- BEGIN leave_admin_mode -->


  <li>
  <a href="{USR_MODE_TGT}" title="{USR_MODE_TITLE}">{USR_MODE_LNK}</a>
  </li>
  <li> </li>
  <!-- END leave_admin_mode -->
  <!-- BEGIN sidebar -->

  <li>
  <a href="{SIDEBAR_TGT}" title="{SIDEBAR_TITLE}">{SIDEBAR_LNK}</a>
  </li>
  <li> </li>
  <!-- END sidebar -->
  <!-- BEGIN upload_pic -->

  <li>
  <a href="{UPL_PIC_TGT}" title="{UPL_PIC_TITLE}">{UPL_PIC_LNK}</a>
  </li>

  <!-- END upload_pic -->
  <!-- BEGIN register -->

  <li>
  <a href="{REGISTER_TGT}" title="{REGISTER_TITLE}">{REGISTER_LNK}</a>
  </li>

  <!-- END register -->
  <!-- BEGIN login -->

  <li>
  <a href="{LOGIN_TGT}" title="{LOGIN_LNK}">{LOGIN_LNK}</a>
  </li>

  <!-- END login -->
  <!-- BEGIN logout -->
  <li>
  <a href="{LOGOUT_TGT}" title="{LOGOUT_LNK}">{LOGOUT_LNK}</a>
  </li>

  <!-- END logout -->
  </ul>


EOT;

// HTML template for sub menu
  if ($CONFIG['browse_by_date'] != 0) {
    $browsebydatebutton = <<< EOT
    <li>
      <a href="{BROWSEBYDATE_TGT}" title="{BROWSEBYDATE_LNK}" rel="nofollow" class="greybox">{BROWSEBYDATE_LNK}</a>
    </li>
EOT;
} else {
    $browsebydatebutton = '';
}

$template_sub_menu = <<< EOT


<ul class="nav">
<!-- BEGIN custom_link -->


<li>
<a href="{CUSTOM_LNK_TGT}" title="{CUSTOM_LNK_TITLE}">{CUSTOM_LNK_LNK}</a>
</li>

<!-- END custom_link -->
<!-- BEGIN album_list -->


<li>
<a href="{ALB_LIST_TGT}" title="{ALB_LIST_TITLE}">{ALB_LIST_LNK}</a>
</li>

<!-- END album_list -->


<li>
<a href="{LASTUP_TGT}" title="{LASTUP_LNK}" rel="nofollow">{LASTUP_LNK}</a>
</li>



<li>
<a href="{LASTCOM_TGT}" title="{LASTCOM_LNK}" rel="nofollow">{LASTCOM_LNK}</a>
</li>



<li>
<a href="{TOPN_TGT}" title="{TOPN_LNK}" rel="nofollow">{TOPN_LNK}</a>
</li>



<li>
<a href="{TOPRATED_TGT}" title="{TOPRATED_LNK}" rel="nofollow">{TOPRATED_LNK}</a>
</li>



<li>
<a href="{FAV_TGT}" title="{FAV_LNK}" rel="nofollow">{FAV_LNK}</a>
</li>



<li>
<a href="{SEARCH_TGT}" title="{SEARCH_LNK}">{SEARCH_LNK}</a>
</li>

</ul>

EOT;





/******************************************************************************
** Section <<<$template_gallery_admin_menu>>> - START
******************************************************************************/
// HTML template for gallery admin menu
$template_gallery_admin_menu = <<<EOT

        <div class="admin_menu_wrapper nav nav-list affix btn-group btn-group-vertical">
          <div class="nav-header"> Menu administracyjne</div>
            <!-- BEGIN admin_approval -->
                <div class="admin_menu btn-inverse btn admin_float" id="admin_menu_anim"><a href="editpics.php?mode=upload_approval" title="{UPL_APP_TITLE}">{UPL_APP_ICO}{UPL_APP_LNK}</a></div>
            <!-- END admin_approval -->
            <!-- BEGIN config -->
                <div class="admin_menu btn-inverse btn admin_float"><a href="admin.php" title="{ADMIN_TITLE}">{ADMIN_ICO}{ADMIN_LNK}</a></div>
            <!-- END config -->
            <!-- BEGIN catmgr -->
                <div class="admin_menu btn-inverse btn admin_float"><a href="catmgr.php" title="{CATEGORIES_TITLE}">{CATEGORIES_ICO}{CATEGORIES_LNK}</a></div>
            <!-- END catmgr -->
            <!-- BEGIN albmgr -->
                <div class="admin_menu btn-inverse btn admin_float"><a href="albmgr.php{CATL}" title="{ALBUMS_TITLE}">{ALBUMS_ICO}{ALBUMS_LNK}</a></div>
            <!-- END albmgr -->
            <!-- BEGIN picmgr -->
                <div class="admin_menu btn-inverse btn admin_float"><a href="picmgr.php" title="{PICTURES_TITLE}">{PICTURES_ICO}{PICTURES_LNK}</a></div>
            <!-- end picmgr -->
            <!-- BEGIN groupmgr -->
                <div class="admin_menu btn-inverse btn admin_float"><a href="groupmgr.php" title="{GROUPS_TITLE}">{GROUPS_ICO}{GROUPS_LNK}</a></div>
            <!-- END groupmgr -->
            <!-- BEGIN usermgr -->
                <div class="admin_menu btn-inverse btn admin_float"><a href="usermgr.php" title="{USERS_TITLE}">{USERS_ICO}{USERS_LNK}</a></div>
            <!-- END usermgr -->
            <!-- BEGIN banmgr -->
                <div class="admin_menu btn-inverse btn admin_float"><a href="banning.php" title="{BAN_TITLE}">{BAN_ICO}{BAN_LNK}</a></div>
            <!-- END banmgr -->
            <!-- BEGIN admin_profile -->
                <div class="admin_menu btn-inverse btn admin_float"><a href="profile.php?op=edit_profile" title="{MY_PROF_TITLE}">{MY_PROF_ICO}{MY_PROF_LNK}</a></div>
            <!-- END admin_profile -->
            <!-- BEGIN review_comments -->
                <div class="admin_menu btn-inverse btn admin_float"><a href="reviewcom.php" title="{COMMENTS_TITLE}">{COMMENTS_ICO}{COMMENTS_LNK}</a></div>
            <!-- END review_comments -->
            <!-- BEGIN log_ecards -->
                <div class="admin_menu btn-inverse btn admin_float"><a href="db_ecard.php" title="{DB_ECARD_TITLE}">{DB_ECARD_ICO}{DB_ECARD_LNK}</a></div>
            <!-- END log_ecards -->
            <!-- BEGIN batch_add -->
                <div class="admin_menu btn-inverse btn admin_float"><a href="searchnew.php" title="{SEARCHNEW_TITLE}">{SEARCHNEW_ICO}{SEARCHNEW_LNK}</a></div>
            <!-- END batch_add -->
            <!-- BEGIN admin_tools -->
                <div class="admin_menu btn-inverse btn admin_float"><a href="util.php?t={TIME_STAMP}#admin_tools" title="{UTIL_TITLE}">{UTIL_ICO}{UTIL_LNK}</a></div>
            <!-- END admin_tools -->
            <!-- BEGIN keyword_manager -->
                <div class="admin_menu btn-inverse btn admin_float"><a href="keywordmgr.php" title="{KEYWORDMGR_TITLE}">{KEYWORDMGR_ICO}{KEYWORDMGR_LNK}</a></div>
            <!-- END keyword_manager -->
            <!-- BEGIN exif_manager -->
                <div class="admin_menu btn-inverse btn admin_float"><a href="exifmgr.php" title="{EXIFMGR_TITLE}">{EXIFMGR_ICO}{EXIFMGR_LNK}</a></div>
            <!-- END exif_manager -->
            <!-- BEGIN plugin_manager -->
                <div class="admin_menu btn-inverse btn admin_float"><a href="pluginmgr.php" title="{PLUGINMGR_TITLE}">{PLUGINMGR_ICO}{PLUGINMGR_LNK}</a></div>
            <!-- END plugin_manager -->
            <!-- BEGIN bridge_manager -->
                <div class="admin_menu btn-inverse btn admin_float"><a href="bridgemgr.php" title="{BRIDGEMGR_TITLE}">{BRIDGEMGR_ICO}{BRIDGEMGR_LNK}</a></div>
            <!-- END bridge_manager -->
            <!-- BEGIN view_log_files -->
                <div class="admin_menu btn-inverse btn admin_float"><a href="viewlog.php" title="{VIEW_LOG_FILES_TITLE}">{VIEW_LOG_FILES_ICO}{VIEW_LOG_FILES_LNK}</a></div>
            <!-- END view_log_files -->
            <!-- BEGIN overall_stats -->
                <div class="admin_menu btn-inverse btn admin_float"><a href="stat_details.php?type=hits&amp;sort=sdate&amp;dir=&amp;sdate=1&amp;ip=1&amp;search_phrase=0&amp;referer=0&amp;browser=1&amp;os=1&amp;mode=fullscreen&amp;page=1&amp;amount=50" title="{OVERALL_STATS_TITLE}">{OVERALL_STATS_ICO}{OVERALL_STATS_LNK}</a></div>
            <!-- END overall_stats -->
            <!-- BEGIN check_versions -->
                <div class="admin_menu btn-inverse btn admin_float"><a href="versioncheck.php" title="{CHECK_VERSIONS_TITLE}">{CHECK_VERSIONS_ICO}{CHECK_VERSIONS_LNK}</a></div>
            <!-- END check_versions -->
            <!-- BEGIN update_database -->
                <div class="admin_menu btn-inverse btn admin_float"><a href="update.php" title="{UPDATE_DATABASE_TITLE}">{UPDATE_DATABASE_ICO}{UPDATE_DATABASE_LNK}</a></div>
            <!-- END update_database -->
            <!-- BEGIN php_info -->
                <div class="admin_menu btn-inverse btn admin_float"><a href="phpinfo.php" title="{PHPINFO_TITLE}">{PHPINFO_ICO}{PHPINFO_LNK}</a></div>
            <!-- END php_info -->
            <!-- BEGIN show_news -->
                <div class="admin_menu btn-inverse btn admin_float"><a href="mode.php?what=news&amp;referer=$REFERER" title="{SHOWNEWS_TITLE}">{SHOWNEWS_ICO}{SHOWNEWS_LNK}</a></div>
            <!-- END show_news -->
            <!-- BEGIN documentation -->
                <div class="admin_menu btn-inverse btn admin_float"><a href="{DOCUMENTATION_HREF}" title="{DOCUMENTATION_TITLE}">{DOCUMENTATION_ICO}{DOCUMENTATION_LNK}</a></div>
            <!-- END documentation -->
            <div style="clear:left;">
            </div>
        </div>

EOT;
/******************************************************************************
** Section <<<$template_gallery_admin_menu>>> - END
******************************************************************************/



/******************************************************************************
** Section <<<$template_img_navbar>>> - START
******************************************************************************/
// HTML template for the image navigation bar
$template_img_navbar = <<<EOT

        <tr>
                <td align="center" valign="middle" class="navmenu" width="48"><a href="{THUMB_TGT}" class="navmenu_pic" title="{THUMB_TITLE}"><img src="{LOCATION}images/navbar/thumbnails.png" align="middle"  alt="{THUMB_TITLE}" /></a></td>
<!-- BEGIN pic_info_button -->
                <!-- button will be added by displayimage.js -->
                <td id="pic_info_button" align="center" valign="middle" class="navmenu" width="48"></td>
<!-- END pic_info_button -->
<!-- BEGIN slideshow_button -->
                <!-- button will be added by displayimage.js -->
                <td id="slideshow_button" align="center" valign="middle" class="navmenu" width="48"></td>
<!-- END slideshow_button -->
                <td align="center" valign="middle" class="navmenu" width="100%">{PIC_POS}</td>
<!-- BEGIN report_file_button -->
                <td align="center" valign="middle" class="navmenu" width="48"><a href="{REPORT_TGT}" class="navmenu_pic" title="{REPORT_TITLE}" rel="nofollow"><img src="{LOCATION}images/navbar/report.png"  align="middle" alt="{REPORT_TITLE}" /></a></td>
<!-- END report_file_button -->
<!-- BEGIN ecard_button -->
                <td align="center" valign="middle" class="navmenu" width="48"><a href="{ECARD_TGT}" class="navmenu_pic" title="{ECARD_TITLE}" rel="nofollow"><img src="{LOCATION}images/navbar/ecard.png"   align="middle" alt="{ECARD_TITLE}" /></a></td>
<!-- END ecard_button -->
<!-- BEGIN nav_start -->
                <td align="center" valign="middle" class="navmenu" width="48"><a href="{START_TGT}" class="navmenu_pic" title="{START_TITLE}"><img src="{LOCATION}images/navbar/{START_IMAGE}"  align="middle" alt="{START_TITLE}" /></a></td>
<!-- END nav_start -->
<!-- BEGIN nav_prev -->
                <td align="center" valign="middle" class="navmenu" width="48"><a href="{PREV_TGT}" class="navmenu_pic" title="{PREV_TITLE}"><img src="{LOCATION}images/navbar/{PREV_IMAGE}"  align="middle" alt="{PREV_TITLE}" /></a></td>
<!-- END nav_prev -->
<!-- BEGIN nav_next -->
                <td align="center" valign="middle" class="navmenu" width="48"><a href="{NEXT_TGT}" class="navmenu_pic" title="{NEXT_TITLE}"><img src="{LOCATION}images/navbar/{NEXT_IMAGE}"  align="middle" alt="{NEXT_TITLE}" /></a></td>
<!-- END nav_next -->
<!-- BEGIN nav_end -->
                <td align="center" valign="middle" class="navmenu" width="48"><a href="{END_TGT}" class="navmenu_pic" title="{END_TITLE}"><img src="{LOCATION}images/navbar/{END_IMAGE}"  align="middle" alt="{END_TITLE}" /></a></td>
<!-- END nav_end -->

        </tr>

EOT;
/******************************************************************************
** Section <<<$template_img_navbar>>> - END
******************************************************************************/



/******************************************************************************
** Section <<<$template_album_admin_menu>>> - START
******************************************************************************/
// HTML template for the ALBUM admin menu displayed in the album list
$template_album_admin_menu = <<<EOT
        <div class="buttonlist align_right">
                <ul class="  btn-group btn-group-vertical">
                        <li class="btn">
                                <a href="delete.php?id={ALBUM_ID}&amp;what=album&amp;form_token={FORM_TOKEN}&amp;timestamp={TIMESTAMP}" class="adm_menu " onclick="return confirm('{CONFIRM_DELETE}');"><span>{DELETE}</span></a>
                        </li>
                        <li class="btn">
                                <a href="modifyalb.php?album={ALBUM_ID}" class="adm_menu "><span>{MODIFY}</span></a>
                        </li>
                        <li class="btn">
                                <a href="editpics.php?album={ALBUM_ID}" class="adm_menu "><span class="last">{EDIT_PICS}</span></a>
                        </li>
                </ul>
        </div>
        <div class="clearer"></div>

EOT;
/******************************************************************************
** Section <<<$template_album_admin_menu>>> - END
******************************************************************************/



/******************************************************************************
** Section <<<$template_user_admin_menu>>> - START
******************************************************************************/
// HTML template for user admin menu
$template_user_admin_menu = <<<EOT

                <div class="span12  ">
                      <ul class="btn-group  btn-group-vertical nav nav-list affix">
                        <li class="btn btn-inverse"  <a href="albmgr.php" title="{ALBMGR_TITLE}">{ALBUMS_ICO}{ALBMGR_LNK}</a> </li>
                        <li class="btn btn-inverse"  <a href="modifyalb.php" title="{MODIFYALB_TITLE}">{MODIFYALB_ICO}{MODIFYALB_LNK}</a> </li>
                        <li class="btn btn-inverse"  <a href="profile.php?op=edit_profile" title="{MY_PROF_TITLE}">{MY_PROF_ICO}{MY_PROF_LNK}</a></li>
                        <li class="btn btn-inverse"  <a href="picmgr.php" title="{PICTURES_TITLE}">{PICTURES_ICO}{PICTURES_LNK}</a> </li>
                      </ul>
                              

                </div>

EOT;
/******************************************************************************
** Section <<<$template_user_admin_menu>>> - END
******************************************************************************/

/******************************************************************************
** Section <<<$template_tab_display>>> - START
******************************************************************************/
// Template used for tabbed display
$template_tab_display = array(
    'left_text'         => '{LEFT_TEXT}' . $LINEBREAK,
    'tab_header'        => '<div class="pagination pagination-centered span12"><ul>',
    'tab_trailer'       => '</ul></div>',
    'active_tab'        => '<li class="active"><span>%d</span></li>',
    'inactive_tab'      => '<li class="disabled"><span><a href="{LINK}">%d</a></span></li>',
    'nav_tab'           => '<li><a href="{LINK}">%s</a></li>' . $LINEBREAK,
    'nav_tab_nolink'    => '<li>%s</li>' . $LINEBREAK,
    'allpages_dropdown' => '<div class="pull-right">%s</div>' . $LINEBREAK,
    'page_gap'          => '<li><a>...</a></li>' . $LINEBREAK,
    'tab_spacer'        => '',
    'page_link'         => '{LINK}',
);
/******************************************************************************
** Section <<<$template_tab_display>>> - END
******************************************************************************/


/******************************************************************************
** Section <<<theme_create_tabs>>> - START
******************************************************************************/
// Function for creating tabs showing multiple pages
function theme_create_tabs($items, $curr_page, $total_pages, $template)
{
    // Tabs do not take into account $lang_text_dir for RTL languages
    global $CONFIG, $lang_create_tabs;

    // Gallery Configuration setting for maximum number of tabs to display
    $maxTab = $CONFIG['max_tabs'];

    if ($total_pages == '') {
        $total_pages = $curr_page;
    }

    if (array_key_exists('page_link',$template)) {
        // Pass through links to tabs with links
        $template['nav_tab']      = strtr($template['nav_tab'], array('{LINK}' => $template['page_link']));
        $template['inactive_tab'] = strtr($template['inactive_tab'], array('{LINK}' => $template['page_link']));
    }

    // Left text, usually shows statistics
    $tabs = sprintf($template['left_text'], $items, $total_pages);
    if (($total_pages == 1)) {
        return $tabs;
    }

    // Header for tabs
    

    if ($CONFIG['tabs_dropdown']) {
        // Dropdown list for all pages
        $tabs_dropdown_js = <<< EOT
            <span id="tabs_dropdown_span"></span>
            <script type="text/javascript"><!--
                $('#tabs_dropdown_span').html('{$lang_create_tabs['jump_to_page']} <select id="tabs_dropdown_select" onchange="if (this.options[this.selectedIndex].value != -1) { window.location.href = this.options[this.selectedIndex].value; }"></select>');
                for (page = 1; page <= $total_pages; page++) {
                    var page_link = '{$template['page_link']}';
                    var selected = '';
                    if (page == $curr_page) {
                        selected = ' selected="selected"';
                    }
                    $('#tabs_dropdown_select').append('<option value="' + page_link.replace( /%d/, page ) + '"' + selected + '>' + page + '</option>');
                }
         --></script>
EOT;
        $tabs .= sprintf($template['allpages_dropdown'], $tabs_dropdown_js);
    }

    $tabs .= $template['tab_header'];

    // Calculate which pages to show on tabs, limited by the maximum number of tabs (set on Gallery Configuration panel)
    if ($total_pages > $maxTab) {
        $start = max(2, $curr_page - floor(($maxTab - 2) / 2));
        $start = min($start, $total_pages - $maxTab + 2);
        $end = $start + $maxTab - 3;
    } else {
        $start = 2;
        $end = $total_pages - 1;
    }

    // Previous page tab
    if ($curr_page != ($start - 1)) {
        $tabs .= sprintf($template['nav_tab'], $curr_page-1, cpg_fetch_icon('tab_left',0,$lang_create_tabs['previous']));
    } else {
        // A previous tab with link is not needed.
        // If you want to show a disabled previous tab,
        //   create an image 'left_inactive.png', put it into themes/YOUR_THEME/images/icons/,
        //   then uncomment the line below.
        // $tabs .= sprintf($template['nav_tab_nolink'], cpg_fetch_icon('left_inactive',0,$lang_create_tabs['previous']));
    }

    // Page 1 tab
    if ($curr_page == 1) {
        $tabs .= sprintf($template['active_tab'], 1);
    } else {
        $tabs .= sprintf($template['inactive_tab'], 1, 1);
    }

    // Gap between page 1 and middle block of tabs
    if ($start > 2) {
        $tabs .= $template['page_gap'];
    }
    $page_gap = ($template['page_gap'] != '');

    // Middle block of tabs
    for ($page = $start ; $page <= $end; $page++) {
        if (!$page_gap || ($page_gap && ($page != $start))) {
            $tabs .= $template['tab_spacer'];
        }
        if ($page == $curr_page) {
            $tabs .= sprintf($template['active_tab'], $page);
        } else {
            $tabs .= sprintf($template['inactive_tab'], $page, $page);
        }
    }

    // Gap between middle block of tabs and last page
    if ($end < $total_pages - 1) {
        $tabs .= $template['page_gap'];
    }

    // Last page tab
    if (!$page_gap) {
        $tabs .= $template['tab_spacer'];
    }
    if ($total_pages > 1) {
        if ($curr_page == $total_pages) {
            $tabs .= sprintf($template['active_tab'], $total_pages);
        } else {
            $tabs .= sprintf($template['inactive_tab'], $total_pages, $total_pages);
        }
    }

    // Next page tab
    if ($curr_page != $total_pages) {
        $tabs .= sprintf($template['nav_tab'], $curr_page + 1, cpg_fetch_icon('tab_right',0,$lang_create_tabs['next']));
    } else {
        // A next tab with link is not needed.
        // If you want to show a disabled next tab,
        //   create an image 'right_inactive.png', put it into themes/YOUR_THEME/images/icons/,
        //   then uncomment the line below.
        // $tabs .= sprintf($template['nav_tab_nolink'], cpg_fetch_icon('right_inactive',0,$lang_create_tabs['next']));
    }

    // Trailer for tabs
    $tabs .= $template['tab_trailer'];

    return $tabs;
}
/******************************************************************************
** Section <<<theme_create_tabs>>> - END
******************************************************************************/




/******************************************************************************
** Section <<<theme_html_img_nav_menu>>> - START
******************************************************************************/
function theme_html_img_nav_menu() {
    global $CONFIG, $CURRENT_PIC_DATA, $meta_nav, $THEME_DIR, $CPG_PHP_SELF, $LINEBREAK; //$PHP_SELF,
    global $album, $cat, $pos, $pic_count, $pic_data, $lang_img_nav_bar, $lang_text_dir, $template_img_navbar;

    $superCage = Inspekt::makeSuperCage();

    $template_img_navbar = CPGPluginAPI::filter('theme_img_navbar', $template_img_navbar);

    $cat_link = is_numeric($album) ? '' : '&amp;cat=' . $cat;
    //$date_link = $_GET['date']=='' ? '' : '&date=' . cpgValidateDate($_GET['date']);

    if ($superCage->get->keyExists('date')) {
      //date will be validated
      $date_link = '&date=' . cpgValidateDate($superCage->get->getRaw('date'));
    } else {
      $date_link = '';
    }

    //$uid_link = is_numeric($_GET['uid']) ? '&amp;uid=' . $_GET['uid'] : '';
    if ($superCage->get->getInt('uid')) {
        $uid_link = '&amp;uid=' . $superCage->get->getInt('uid');
    } else {
        $uid_link = '';
    }

    $human_pos = $pos + 1;
    $page = ceil(($pos + 1) / ($CONFIG['thumbrows'] * $CONFIG['thumbcols']));
    $pid = $CURRENT_PIC_DATA['pid'];

    if ($pos > 0) {
        $start = 0;
        //$start_tgt = "{$_SERVER['PHP_SELF']}?album=$album$cat_link&amp;pos=$start"; // Abbas - added pid in URL instead of pos
        $start_tgt = "$CPG_PHP_SELF?album=$album$cat_link$date_link&amp;pid={$pic_data[$start]['pid']}";
        $start_title = $lang_img_nav_bar['go_album_start'];
        $meta_nav .= "<link rel=\"start\" href=\"$start_tgt\" title=\"$start_title\" />" . $LINEBREAK;
        $start_image = (($lang_text_dir == 'ltr') ? 'start.png' : 'end.png');

        $prev = $pos - 1;
        //$prev_tgt = "{$_SERVER['PHP_SELF']}?album=$album$cat_link&amp;pos=$prev$uid_link";// Abbas - added pid in URL instead of pos
        if ($album == 'lastcom' || $album == 'lastcomby') {
            $page = cpg_get_comment_page_number($pic_data[$prev]['msg_id']);
            $page = (is_numeric($page)) ? "&amp;page=$page" : '';
            $prev_tgt = "$CPG_PHP_SELF?album=$album$cat_link$date_link&amp;pid={$pic_data[$prev]['pid']}$uid_link&amp;msg_id={$pic_data[$prev]['msg_id']}$page#comment{$pic_data[$prev]['msg_id']}";
            $start_tgt .= "$uid_link&amp;msg_id={$pic_data[$start]['msg_id']}$page#comment{$pic_data[$start]['msg_id']}";
        } else {
            $prev_tgt = "$CPG_PHP_SELF?album=$album$cat_link$date_link&amp;pid={$pic_data[$prev]['pid']}$uid_link#top_display_media";
            $start_tgt .= "$uid_link#top_display_media";
        }
        $prev_title = $lang_img_nav_bar['prev_title'];
        $meta_nav .= "<link rel=\"prev\" href=\"$prev_tgt\" title=\"$prev_title\" />" . $LINEBREAK;
        $prev_image = (($lang_text_dir == 'ltr') ? 'prev.png' : 'next.png');
    } else {
        // on first image, so no previous button/link
        $prev_tgt = "javascript:;";
        $prev_title = "";
        $prev_image = (($lang_text_dir == 'ltr') ? 'prev_inactive.png' : 'next_inactive.png');
        $start_tgt = "javascript:;";
        $start_title = "";
        $start_image = (($lang_text_dir == 'ltr') ? 'start_inactive.png' : 'end_inactive.png');
    }

    if ($pos < ($pic_count -1)) {
        $end = $pic_count - 1;
        //$end_tgt = "{$_SERVER['PHP_SELF']}?album=$album$cat_link&amp;pos=$end";// Abbas - added pid in URL instead of pos
        $end_tgt = "$CPG_PHP_SELF?album=$album$cat_link$date_link&amp;pid={$pic_data[$end]['pid']}";
        $end_title = $lang_img_nav_bar['go_album_end'];
        $meta_nav .= "<link rel=\"last\" href=\"$end_tgt\" title=\"$end_title\" />" . $LINEBREAK;
        $end_image = (($lang_text_dir == 'ltr') ? 'end.png' : 'start.png');

        $next = $pos + 1;
        //$next_tgt = "{$_SERVER['PHP_SELF']}?album=$album$cat_link&amp;pos=$next$uid_link";// Abbas - added pid in URL instead of pos
        if ($album == 'lastcom' || $album == 'lastcomby') {
            $page = cpg_get_comment_page_number($pic_data[$next]['msg_id']);
            $page = (is_numeric($page)) ? "&amp;page=$page" : '';
            $next_tgt = "$CPG_PHP_SELF?album=$album$cat_link$date_link&amp;pid={$pic_data[$next]['pid']}$uid_link&amp;msg_id={$pic_data[$next]['msg_id']}$page#comment{$pic_data[$next]['msg_id']}";
            $end_tgt .= "$uid_link&amp;msg_id={$pic_data[$end]['msg_id']}$page#comment{$pic_data[$end]['msg_id']}";
        } else {
            $next_tgt = "$CPG_PHP_SELF?album=$album$cat_link$date_link&amp;pid={$pic_data[$next]['pid']}$uid_link#top_display_media";
            $end_tgt .= "$uid_link#top_display_media";
        }
        $next_title = $lang_img_nav_bar['next_title'];
        $meta_nav .= "<link rel=\"next\" href=\"$next_tgt\" title=\"$next_title\"/>" . $LINEBREAK;
        $next_image = (($lang_text_dir == 'ltr') ? 'next.png' : 'prev.png');
    } else {
        // on last image, so no next button/link
        $next_tgt = "javascript:;";
        $next_title = "";
        $next_image = (($lang_text_dir == 'ltr') ? 'next_inactive.png' : 'prev_inactive.png');
        $end_tgt = "javascript:;";
        $end_title = "";
        $end_image = (($lang_text_dir == 'ltr') ? 'end_inactive.png' : 'start_inactive.png');
    }

    if (USER_CAN_SEND_ECARDS) {
        $ecard_tgt = "ecard.php?album=$album$cat_link$date_link&amp;pid=$pid&amp;pos=$pos";
        $ecard_title = $lang_img_nav_bar['ecard_title'];
    } else {
        template_extract_block($template_img_navbar, 'ecard_button'); // added to remove button if cannot send ecard
        /*
        $ecard_tgt = "javascript:alert('" . addslashes($lang_img_nav_bar['ecard_disabled_msg']) . "');";
        $ecard_title = $lang_img_nav_bar['ecard_disabled'];
        */
    }

    // report to moderator buttons
    $report_tgt = '';
    if (($CONFIG['report_post']==1) && (USER_CAN_SEND_ECARDS)) {
        $report_tgt = "report_file.php?album=$album$cat_link$date_link&amp;pid=$pid&amp;pos=$pos";
    } else {
        // remove button if report toggle is off
        template_extract_block($template_img_navbar, 'report_file_button');
    }

    $thumb_tgt = "thumbnails.php?album=$album$cat_link$date_link&amp;page=$page$uid_link";
    $meta_nav .= "<link rel=\"up\" href=\"$thumb_tgt\" title=\"".$lang_img_nav_bar['thumb_title']."\"/>" . $LINEBREAK;

    // needed when viewing slideshow of meta albums lastcom/lastcomby
    $msg_id = ($album == 'lastcom' || $album == 'lastcomby') ? "&amp;msg_id={$pic_data[$pos]['msg_id']}&amp;page=$page" : '';

    $slideshow_tgt = "$CPG_PHP_SELF?album=$album$cat_link$date_link$uid_link&amp;pid=$pid$msg_id&amp;slideshow=".$CONFIG['slideshow_interval'].'#top_display_media';

    // if set, this will override the default slideshow button to be inserted by displayimage.js
    $slideshow_btn = '';
    // if set, this will override the default pic_info button to be inserted by displayimage.js
    $pic_info_btn = '';

    $pic_pos = sprintf($lang_img_nav_bar['pic_pos'], $human_pos, $pic_count);

    if (defined('THEME_HAS_NAVBAR_GRAPHICS')) {
        $location = $THEME_DIR;
    } else {
        $location = '';
    }
    // add javascript vars
    $js_buttons = array(
        'pic_info_title'  => $lang_img_nav_bar['pic_info_title'],
        'pic_info_btn'    => $pic_info_btn,
        'slideshow_tgt'   => $slideshow_tgt,
        'slideshow_title' => $lang_img_nav_bar['slideshow_title'],
        'slideshow_btn'   => $slideshow_btn,
        'loc' => $location,
    );
    set_js_var('buttons', $js_buttons);

    $params = array(
        '{THUMB_TGT}' => $thumb_tgt,
        '{THUMB_TITLE}' => $lang_img_nav_bar['thumb_title'],
        '{PIC_POS}' => $pic_pos,
        '{ECARD_TGT}' => $ecard_tgt,
        '{ECARD_TITLE}' => $ecard_title,
        '{START_TGT}' => $start_tgt,
        '{START_TITLE}' => $start_title,
        '{START_IMAGE}' => $start_image,
        '{PREV_TGT}' => $prev_tgt,
        '{PREV_TITLE}' => $prev_title,
        '{PREV_IMAGE}' => $prev_image,
        '{NEXT_TGT}' => $next_tgt,
        '{NEXT_TITLE}' => $next_title,
        '{NEXT_IMAGE}' => $next_image,
        '{END_TGT}' => $end_tgt,
        '{END_TITLE}' => $end_title,
        '{END_IMAGE}' => $end_image,
        '{REPORT_TGT}' => $report_tgt,
        '{REPORT_TITLE}' => $lang_img_nav_bar['report_title'],
        '{LOCATION}' => $location,
    );

    return template_eval($template_img_navbar, $params);
}
/******************************************************************************
** Section <<<theme_html_img_nav_menu>>> - END
******************************************************************************/


?>

