<?php


/******************************************************************************
** Section <<<$template_image_rating>>> - START
******************************************************************************/
// HTML template for the image rating box
$template_image_rating = <<<EOT
<div class="span12">
<span  id="voting_title"><Strong> {TITLE}</strong> {VOTES}</span>
<table cellspacing="1" cellpadding="0" class="maintable">

    <tr id="rating_stars">
        <td class="tableb" id="star_rating"></td>
    </tr>
    <tr class="noscript">
      <td class="tableb" colspan="6" align="center">
        <noscript>
           <small>{JS_WARNING}</small>
        </noscript>
      </td>
    </tr>
</table>
</div>
EOT;
/******************************************************************************
** Section <<<$template_image_rating>>> - END
******************************************************************************/



/******************************************************************************
** Section <<<$template_film_strip>>> - START
******************************************************************************/
// HTML template for filmstrip display
$template_film_strip = <<<EOT
<div id="film " >
    <ul class="thumbnails film-strip ">
        <li  class="prev_strip">          </li>
          {THUMB_STRIP}
        <li class="next_strip">          </li>
    </ul>
</div>

<!-- BEGIN thumb_cell -->
                <li class=" thumb " >
                    <div class="thumbnail" >
                        <a href="{LINK_TGT}" class="thumbLink" >{THUMB}</a>
                    </div>
                </li>

<!-- END thumb_cell -->
<!-- BEGIN empty_cell -->
                <li>&nbsp;</li>
<!-- END empty_cell -->

EOT;
/******************************************************************************
** Section <<<$template_film_strip>>> - END
******************************************************************************/


/******************************************************************************
** Section <<<$template_display_media>>> - START
******************************************************************************/
// HTML template for intermediate image display
$template_display_media = <<<EOT
<div class="span9 text-center" style="margin-left:0px;">
    {IMAGE}
</div>
<!-- BEGIN img_desc -->
<!-- BEGIN title -->
<!-- END title -->
<!-- BEGIN caption -->  
<!-- END caption -->
<!-- END img_desc --> 
EOT;
/******************************************************************************
** Section <<<$template_display_media>>> - END
******************************************************************************/









/******************************************************************************
** Section <<<theme_display_image>>> - START
******************************************************************************/
function theme_display_image($nav_menu, $picture, $votes, $pic_info, $comments, $film_strip)
{
    global $CONFIG, $LINEBREAK,$template_img_navbar,$CURRENT_ALBUM_DATA,$CURRENT_PIC_DATA,$pic_count;
    global $album, $cat, $pos, $pic_count, $pic_data, $lang_img_nav_bar, $lang_text_dir, $template_img_navbar,$lang_display_comments;

    $superCage = Inspekt::makeSuperCage();
    $picinfo = $superCage->cookie->keyExists('picinfo') ? $superCage->cookie->getAlpha('picinfo') : ($CONFIG['display_pic_info'] ? 'block' : 'none');

    $width = $CONFIG['picture_table_width'];
    
    //pkk prev and next links
    $human_pos = $pos + 1;
    $page = ceil(($pos + 1) / ($CONFIG['thumbrows'] * $CONFIG['thumbcols']));
    $pid = $CURRENT_PIC_DATA['pid'];

    if ($pos > 0) {
        $start = 0;
        //$start_tgt = "{$_SERVER['PHP_SELF']}?album=$album$cat_link&amp;pos=$start"; // Abbas - added pid in URL instead of pos
      
        $prev = $pos - 1;
        //$prev_tgt = "{$_SERVER['PHP_SELF']}?album=$album$cat_link&amp;pos=$prev$uid_link";// Abbas - added pid in URL instead of pos
        if ($album == 'lastcom' || $album == 'lastcomby') {
            $page = cpg_get_comment_page_number($pic_data[$prev]['msg_id']);
            $page = (is_numeric($page)) ? "&amp;page=$page" : '';
            $prev_tgt = "$CPG_PHP_SELF?album=$album$cat_link$date_link&amp;pid={$pic_data[$prev]['pid']}$uid_link&amp;msg_id={$pic_data[$prev]['msg_id']}$page#comment{$pic_data[$prev]['msg_id']}";

        } else {
            $prev_tgt = "$CPG_PHP_SELF?album=$album$cat_link$date_link&amp;pid={$pic_data[$prev]['pid']}$uid_link#top_display_media";

        }
        $prev_title = $lang_img_nav_bar['prev_title'];

    } else {
        // on first image, so no previous button/link
        $prev_tgt = "javascript:;";
        $prev_title = "";
    }

    if ($pos < ($pic_count -1)) {
        $end = $pic_count - 1;
        //$end_tgt = "{$_SERVER['PHP_SELF']}?album=$album$cat_link&amp;pos=$end";// Abbas - added pid in URL instead of pos
        $next = $pos + 1;
        //$next_tgt = "{$_SERVER['PHP_SELF']}?album=$album$cat_link&amp;pos=$next$uid_link";// Abbas - added pid in URL instead of pos
        if ($album == 'lastcom' || $album == 'lastcomby') {
            $page = cpg_get_comment_page_number($pic_data[$next]['msg_id']);
            $page = (is_numeric($page)) ? "&amp;page=$page" : '';
            $next_tgt = "$CPG_PHP_SELF?album=$album$cat_link$date_link&amp;pid={$pic_data[$next]['pid']}$uid_link&amp;msg_id={$pic_data[$next]['msg_id']}$page#comment{$pic_data[$next]['msg_id']}";
        } else {
            $next_tgt = "$CPG_PHP_SELF?album=$album$cat_link$date_link&amp;pid={$pic_data[$next]['pid']}$uid_link#top_display_media";
        }
        $next_title = $lang_img_nav_bar['next_title'];
    } else {
        // on last image, so no next button/link
        $next_tgt = "javascript:;";
        $next_title = "";
    }


    starttable(); 

    echo '<div class="span12" itemscope itemtype="http://schema.org/ImageObject"><div class="row-fluid row-title">';  
    echo '<a name="top_display_media" class="clearfix"></a>';
    if(!empty($CURRENT_PIC_DATA['title'])) {
        echo '<h1 itemprop="name" class="pic_title span10 ">'.$CURRENT_PIC_DATA['title'].'</h1>';
    } else {
        echo '<h1 itemprop="name" class="pic_title span10 ">'.$CURRENT_ALBUM_DATA['title'].'</h1>';
    }
    if(!empty($CURRENT_PIC_DATA['caption'])) {
        echo '<h2 class="pic_title pic_caption span10  " itemprop="description">'.$CURRENT_PIC_DATA['caption'].'</h2> ';
    }
   
    echo '</div><div class="main row-fluid"><div class="row-nav row-fluid "><div class="span3 offset3 text-center"><a class="img-nav btn btn-inverse " href="'.$prev_tgt.'" title="'. $prev_title .'"> « </a>';
    echo '<span class="">&nbsp;'.  sprintf($lang_img_nav_bar['pic_pos'], '<big><b>'.$human_pos.'</b></big>', $pic_count).'&nbsp;</span> ';         
    echo '<a  class="img-nav btn  btn-inverse "   href="'.$next_tgt.'" title="'. $next_title .'"> » </a></div></div>';
    echo $picture;
    echo '<div class="span3 right-column">'.$votes. '<div class="clearfix"></div>'; //$nav_menu;
    echo $pic_info.'</div></div>';


   if ($CONFIG['display_film_strip'] == 1) {
        echo '<br/></div><div class="row-fluid row-filmstrip" style="margin-left:0px;"><h4><b>'.$CURRENT_ALBUM_DATA['title'].' :</b></h4>';
        echo '<a class="img-nav btn btn-inverse" href="'.$prev_tgt.'" title="'. $prev_title .'"> « </a> ';
        echo $film_strip;
        echo '<a  class="img-nav btn  btn-inverse "  href="'.$next_tgt.'" title="'. $next_title .'"> » </a>';
   
    } 

    echo '<br/></div><div class="row-fluid" id="comments">';
    echo '<a name="comments_top"></a>';
    echo $comments;

    echo '</div>' . $LINEBREAK;
    //echo $nav_menu;
    echo '</div>';

   
    endtable();


}
/******************************************************************************
** Section <<<theme_display_image>>> - END
******************************************************************************/



/******************************************************************************
** Section <<<theme_html_picinfo>>> - START
******************************************************************************/
function theme_html_picinfo(&$info)
{
    global $lang_picinfo, $CONFIG, $CURRENT_PIC_DATA, $LINEBREAK;

    if ($CONFIG['picinfo_movie_download_link']) {
        $path_to_pic = $CONFIG['fullpath'] . $CURRENT_PIC_DATA['filepath'] . $CURRENT_PIC_DATA['filename'];
        $mime_content = cpg_get_type($CURRENT_PIC_DATA['filename']);
        if ($mime_content['content']=='movie') {
            $info[$lang_picinfo['download_URL']] = '<a href="' . $CONFIG["ecards_more_pic_target"] . (substr($CONFIG["ecards_more_pic_target"], -1) == '/' ? '' : '/') . $path_to_pic.'">'. $lang_picinfo['movie_player'] .'</a>';
        }
    }


    $out  .= '<div class="row-fluid">';
    $template =  '<h4 class="row-fluid">%s :</h4><div>%s</div><br/>';

    
    $dontShow = array("URL","Dimensions", "Filesize", "Filename");

    foreach ($info as $key => $value) {
          
          if(!in_array($key,$dontShow)) {
            $out .= sprintf($template, $key, $value);
          }
      }
    $out  .= '</div>';

    return $out;
}
/******************************************************************************
** Section <<<theme_html_picinfo>>> - END
******************************************************************************/


/******************************************************************************
** Section <<<theme_display_film_strip>>> - START
******************************************************************************/
// Function to display the film strip
function theme_display_film_strip(&$thumb_list, $nbThumb, $album_name, $aid, $cat, $pos, $sort_options, $mode = 'thumb', $date='', $filmstrip_prev_pos, $filmstrip_next_pos,$max_block_items,$thumb_width)
{
    global $CONFIG, $THEME_DIR, $CURRENT_PIC_DATA;
    global $template_film_strip, $lang_film_strip, $lang_common, $pic_count,$mar_pic;

    $superCage = Inspekt::makeSuperCage();

    static $template = '';
    static $thumb_cell = '';
    static $empty_cell = '';
    static $spacer = '';

    if ((!$template)) {
        $template = $template_film_strip;
        $thumb_cell = str_replace('<td', '<td align="center"', template_extract_block($template, 'thumb_cell')); // center thumbnails
        $empty_cell = template_extract_block($template, 'empty_cell');
    }

    $cat_link = is_numeric($aid) ? '' : '&amp;cat=' . $cat;
    $date_link = $date=='' ? '' : '&amp;date=' . $date;

    if ($superCage->get->getInt('uid')) {
        $uid_link = '&amp;uid=' . $superCage->get->getInt('uid');
    } else {
        $uid_link = '';
    }

    $i = 0;
    $thumb_strip = '';
    foreach($thumb_list as $thumb) {
        $i++;
        if ($mode == 'thumb') { 
            if ($thumb['pos'] == $pos && !$superCage->get->keyExists('film_strip')) {
                $thumb['image'] = str_replace('class="image"', 'class="image middlethumb"', $thumb['image']);
            }
            if ($CURRENT_PIC_DATA['pid']==$thumb['pid']) {
                 $thumb['image'] = str_replace('class="strip_image"', 'class="strip_image active"', $thumb['image']);
            }
            // determine if thumbnail link targets should open in a pop-up
            if ($CONFIG['thumbnail_to_fullsize'] == 1) { // code for full-size pop-up
                if (!USER_ID && $CONFIG['allow_unlogged_access'] <= 2) {
                    $target = 'javascript:;" onclick="alert(\''.sprintf($lang_errors['login_needed'],'','','','').'\');';
                } elseif (USER_ID && USER_ACCESS_LEVEL <= 2) {
                    $target = 'javascript:;" onclick="alert(\''.sprintf($lang_errors['access_intermediate_only'],'','','','').'\');';
                } else {
                    $target = 'javascript:;" onclick="MM_openBrWindow(\'displayimage.php?pid=' . $thumb['pid'] . '&fullsize=1\',\'' . uniqid(rand()) . '\',\'scrollbars=yes,toolbar=no,status=no,resizable=yes,width=' . ((int)$thumb['pwidth']+(int)$CONFIG['fullsize_padding_x']) .  ',height=' .   ((int)$thumb['pheight']+(int)$CONFIG['fullsize_padding_y']). '\');';
                }
            } elseif ($aid == 'lastcom' || $aid == 'lastcomby') {
                $page = cpg_get_comment_page_number($thumb['msg_id']);
                $page = (is_numeric($page)) ? "&amp;page=$page" : '';
                $target = "displayimage.php?album=$aid$cat_link$date_link&amp;pid={$thumb['pid']}$uid_link&amp;msg_id={$thumb['msg_id']}$page#comment{$thumb['msg_id']}";
            } else {
                $target = "displayimage.php?album=$aid$cat_link$date_link&amp;pid={$thumb['pid']}$uid_link#top_display_media";
            }
            $params = array(
                '{LINK_TGT}' => $target,
                '{THUMB}' => $thumb['image'],
                '{ONE_WIDTH}'  => "width:".$thumb_width."px; float: left" ,
                );
        } else {
            $params = array(
                '{LINK_TGT}' => "index.php?cat={$thumb['cat']}",
                '{THUMB}' => $thumb['image'],
                '{ONE_WIDTH}'  => "width:".$thumb_width."px; float: left" ,
                );
        }
        $thumb_strip .= template_eval($thumb_cell, $params);
    }

    if (defined('THEME_HAS_FILM_STRIP_GRAPHICS')) {
        $tile1 = $THEME_DIR . 'images/tile1.gif';
        $tile2 = $THEME_DIR . 'images/tile2.gif';
    } elseif (defined('THEME_HAS_FILM_STRIP_GRAPHIC')) {
        $tile1=$tile2=$THEME_DIR . 'images/tile.gif';
    } else {
        $tile1=$tile2= 'images/tile.gif';
    }

    if (defined('THEME_HAS_NAVBAR_GRAPHICS')) {
        $location = $THEME_DIR;
    } else {
        $location= '';
    }
    $max_itme_width_ul = $max_block_items;
    if(($max_block_items%2)==0){
        $max_itme_width_ul = $max_block_items +1;
    }
    $set_width_to_film = "width:".($max_block_items*($thumb_width+4))."px; position:relative;";

    $params = array('{THUMB_STRIP}' => $thumb_strip,
        '{COLS}' => $i,
        '{TILE1}' => $tile1,
        '{TILE2}' => $tile2,
        '{SET_WIDTH}'  => $set_width_to_film,
        );

    ob_start();
    echo '<div id="filmstrip" class="span7  text-center">';
    //starttable($CONFIG['picture_table_width']);
    echo template_eval($template, $params);
   //endtable();
    echo '</div>';
    $film_strip = ob_get_contents();
    ob_end_clean();

    return $film_strip;
}
/******************************************************************************
** Section <<<theme_display_film_strip>>> - END
******************************************************************************/


/******************************************************************************
** Section <<<theme_html_picture>>> - START
******************************************************************************/
// Displays a picture
function theme_html_picture()
{
    global $CONFIG, $CURRENT_PIC_DATA, $CURRENT_ALBUM_DATA, $USER, $LINEBREAK;
    global $album, $lang_date, $template_display_media;
    global $lang_display_image_php, $lang_picinfo, $lang_common, $lang_errors;

    $superCage = Inspekt::makeSuperCage();

    $pid = $CURRENT_PIC_DATA['pid'];
    $pic_title = '';

    if (!isset($USER['liv']) || !is_array($USER['liv'])) {
        $USER['liv'] = array();
    }
    // Add 1 to hit counter
    if ((!USER_IS_ADMIN && $CONFIG['count_admin_hits'] == 0 || $CONFIG['count_admin_hits'] == 1) && !in_array($pid, $USER['liv']) && $superCage->cookie->keyExists($CONFIG['cookie_name'] . '_data')) {
        add_hit($pid);
        if (count($USER['liv']) > 4) array_shift($USER['liv']);
        array_push($USER['liv'], $pid);
    }

    // The weird comparision is because only picture_width is stored
    if ($CONFIG['thumb_use']=='ht' && $CURRENT_PIC_DATA['pheight'] > $CONFIG['picture_width'] ) {
        $condition = true;
    } elseif ($CONFIG['thumb_use']=='wd' && $CURRENT_PIC_DATA['pwidth'] > $CONFIG['picture_width']) {
        $condition = true;
    } elseif ($CONFIG['thumb_use']=='any' && max($CURRENT_PIC_DATA['pwidth'], $CURRENT_PIC_DATA['pheight']) > $CONFIG['picture_width']) {
        $condition = true;
        //thumb cropping
    } elseif ($CONFIG['thumb_use']=='ex' && max($CURRENT_PIC_DATA['pwidth'], $CURRENT_PIC_DATA['pheight']) > $CONFIG['picture_width']) {
        $condition = true;
    } else {
        $condition = false;
    }

    if ($CURRENT_PIC_DATA['title'] != '') {
        $pic_title .= $CURRENT_PIC_DATA['title'] . $LINEBREAK;
    }
    if ($CURRENT_PIC_DATA['caption'] != '') {
        $pic_title .= $CURRENT_PIC_DATA['caption'] . $LINEBREAK;
    }
    if ($CURRENT_PIC_DATA['keywords'] != '') {
        $pic_title .= $lang_common['keywords'] . ": " . $CURRENT_PIC_DATA['keywords'];
    }

    if (!$CURRENT_PIC_DATA['title'] && !$CURRENT_PIC_DATA['caption']) {
        template_extract_block($template_display_media, 'img_desc');
    } else {
        if (!$CURRENT_PIC_DATA['title']) {
            template_extract_block($template_display_media, 'title');
        }
        if (!$CURRENT_PIC_DATA['caption']) {
            template_extract_block($template_display_media, 'caption');
        }
    }

    $CURRENT_PIC_DATA['menu'] = html_picture_menu(); //((USER_ADMIN_MODE && $CURRENT_ALBUM_DATA['category'] == FIRST_USER_CAT + USER_ID) || ($CONFIG['users_can_edit_pics'] && $CURRENT_PIC_DATA['owner_id'] == USER_ID && USER_ID != 0) || GALLERY_ADMIN_MODE) ? html_picture_menu($pid) : '';

    $image_size = array();

    if ($CONFIG['make_intermediate'] && $condition ) {
        $picture_url = get_pic_url($CURRENT_PIC_DATA, 'normal');
    } else {
        $picture_url = get_pic_url($CURRENT_PIC_DATA, 'fullsize');
    }

    list($image_size['width'], $image_size['height'], , $image_size['geom']) = cpg_getimagesize(urldecode($picture_url));

    $pic_title = '';
    $mime_content = cpg_get_type($CURRENT_PIC_DATA['filename']);

    if ($mime_content['content']=='movie' || $mime_content['content']=='audio') {

        if ($CURRENT_PIC_DATA['pwidth']==0 || $CURRENT_PIC_DATA['pheight']==0) {
            $resize_method = $CONFIG['picture_use'] == "thumb" ? ($CONFIG['thumb_use'] == "ex" ? "any" : $CONFIG['thumb_use']) : $CONFIG['picture_use'];
            if ($resize_method == 'ht') {
                $pwidth = $CONFIG['picture_width']*4/3;
                $pheight = $CONFIG['picture_width'];
            } else { 
                $pwidth = $CONFIG['picture_width'];
                $pheight = $CONFIG['picture_width']*3/4;
            }

            $CURRENT_PIC_DATA['pwidth']  = $pwidth; // Default width

            // Set default height; if file is a movie
            if ($mime_content['content']=='movie') {
                $CURRENT_PIC_DATA['pheight'] = $pheight; // Default height
            }
        }

        $ctrl_offset['mov']=15;
        $ctrl_offset['wmv']=45;
        $ctrl_offset['swf']=0;
        $ctrl_offset['rm']=0;
        $ctrl_offset_default=45;
        $ctrl_height = (isset($ctrl_offset[$mime_content['extension']]))?($ctrl_offset[$mime_content['extension']]):$ctrl_offset_default;
        $image_size['whole']='width="'.$CURRENT_PIC_DATA['pwidth'].'" height="'.($CURRENT_PIC_DATA['pheight']+$ctrl_height).'"';
    }

    if ($mime_content['content']=='image') {
        if ($CURRENT_PIC_DATA['mode'] != 'fullsize') {
            $winsizeX = $CURRENT_PIC_DATA['pwidth'] + $CONFIG['fullsize_padding_x'];  //the +'s are the mysterious FF and IE paddings
            $winsizeY = $CURRENT_PIC_DATA['pheight'] + $CONFIG['fullsize_padding_y']; //the +'s are the mysterious FF and IE paddings
            if ($CONFIG['transparent_overlay'] == 1) {
                $pic_html = "<table cellpadding=\"0\" cellspacing=\"0\" ><tr><td background=\"" . $picture_url . "\" width=\"{$image_size['width']}\" height=\"{$image_size['height']}\" class=\"image\">";
                $pic_html_href_close = '</a>' . $LINEBREAK;
                if (!USER_ID && $CONFIG['allow_unlogged_access'] <= 2) {
                    if ($CONFIG['allow_user_registration'] == 0) {
                        $pic_html_href_close = '';
                    } else {
                        $pic_html .= '<a href="javascript:;" onclick="alert(\''.sprintf($lang_errors['login_needed'],'','','','').'\');">';
                    }
                } elseif (USER_ID && USER_ACCESS_LEVEL <= 2) {
                    $pic_html .= '<a href="javascript:;" onclick="alert(\''.sprintf($lang_errors['access_intermediate_only'],'','','','').'\');">';
                } else {
                    $pic_html .= "<a href=\"javascript:;\" onclick=\"MM_openBrWindow('displayimage.php?pid=$pid&amp;fullsize=1','" . uniqid(rand()) . "','scrollbars=yes,toolbar=no,status=no,resizable=yes,width=$winsizeX,height=$winsizeY')\">";
                }
                $pic_title = $lang_display_image_php['view_fs'] . $LINEBREAK . '==============' . $LINEBREAK . $pic_title;
                $pic_html .= "<img src=\"images/image.gif?id=".floor(rand()*1000+rand())."\" width=\"{$image_size['width']}\" height=\"{$image_size['height']}\"   alt=\"{$lang_display_image_php['view_fs']}\" /><br />";
                $pic_html .= $pic_html_href_close . '</td></tr></table>';
                //PLUGIN FILTER
                $pic_html = CPGPluginAPI::filter('html_image_reduced_overlay', $pic_html);
            } else {
                $pic_html_href_close = '</a>' . $LINEBREAK;
                if (!USER_ID && $CONFIG['allow_unlogged_access'] <= 2) {
                    if ($CONFIG['allow_user_registration'] == 0) {
                        $pic_html = $pic_html_href_close = '';
                    } else {
                        $pic_html = '<a href="javascript:;" onclick="alert(\''.sprintf($lang_errors['login_needed'],'','','','').'\');">';
                    }
                } elseif (USER_ID && USER_ACCESS_LEVEL <= 2) {
                    $pic_html = '<a href="javascript:;" onclick="alert(\''.sprintf($lang_errors['access_intermediate_only'],'','','','').'\');">';
                } else {
                    $pic_html = "<a href=\"javascript:;\" onclick=\"MM_openBrWindow('displayimage.php?pid=$pid&amp;fullsize=1','" . uniqid(rand()) . "','scrollbars=yes,toolbar=no,status=no,resizable=yes,width=$winsizeX,height=$winsizeY')\">";
                }
                $pic_title = $lang_display_image_php['view_fs'] . $LINEBREAK . '==============' . $LINEBREAK . $pic_title;
                $pic_html .= "<img src=\"" . $picture_url . "\" {$image_size['geom']} class=\"image\"  alt=\"{$lang_display_image_php['view_fs']}\" /><br />";
                $pic_html .= $pic_html_href_close;
                //PLUGIN FILTER
                $pic_html = CPGPluginAPI::filter('html_image_reduced', $pic_html);
            }
        } else {
            if ($CONFIG['transparent_overlay'] == 1) {
                $pic_html = "<table cellpadding=\"0\" cellspacing=\"0\" ><tr><td background=\"" . $picture_url . "\" width=\"{$CURRENT_PIC_DATA['pwidth']}\" height=\"{$CURRENT_PIC_DATA['pheight']}\" class=\"image\">";
                $pic_html .= "<img src=\"images/image.gif?id=".floor(rand()*1000+rand())."\" width={$CURRENT_PIC_DATA['pwidth']} height={$CURRENT_PIC_DATA['pheight']}  alt=\"\" /><br />" . $LINEBREAK;
                $pic_html .= "</td></tr></table>";
                //PLUGIN FILTER
                $pic_html = CPGPluginAPI::filter('html_image_overlay', $pic_html);
            } else {
                $pic_html = "<img src=\"" . $picture_url . "\" {$image_size['geom']} class=\"image\"  alt=\"\" /><br />" . $LINEBREAK;
                //PLUGIN FILTER
                $pic_html = CPGPluginAPI::filter('html_image', $pic_html);
            }
        }
    } elseif ($mime_content['content']=='document') {
        $pic_thumb_url = get_pic_url($CURRENT_PIC_DATA,'thumb');
        $pic_html = "<a href=\"{$picture_url}\" target=\"_blank\" class=\"document_link\"><img src=\"".$pic_thumb_url."\"  class=\"image\" /></a><br />" . $LINEBREAK;
        //PLUGIN FILTER
        $pic_html = CPGPluginAPI::filter('html_document', $pic_html);
    } else {
        $autostart = ($CONFIG['media_autostart']) ? ('true'):('false');

        if ($mime_content['player'] == 'HTMLA') {
            $pic_html  = '<audio controls="true" src="' . $picture_url . '" autostart="' . $autostart . '"></audio>';
        } elseif ($mime_content['player'] == 'HTMLV') {
            $pic_html  = '<video controls="true" src="' . $picture_url . '" autostart="' . $autostart . '"' . $image_size['whole'] . '></video>';
        } else {

            $players['WMP'] = array('id' => 'MediaPlayer',
                                    'clsid' => 'classid="clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95" ',
                                    'codebase' => 'codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=5,1,52,701" ',
                                    'mime' => 'type="application/x-mplayer2" ',
                                   );
            $players['DIVX'] = array('id' => 'DivX',
                                    'clsid' => 'classid="clsid:67DABFBF-D0AB-41fa-9C46-CC0F21721616"',
                                    'codebase' => 'codebase="http://go.divx.com/plugin/DivXBrowserPlugin.cab"',
                                    'mime' => 'type="video/divx"'
                                   );
            $players['RMP'] = array('id' => 'RealPlayer',
                                    'clsid' => 'classid="clsid:CFCDAA03-8BE4-11cf-B84B-0020AFBBCCFA" ',
                                    'codebase' => '',
                                    'mime' => 'type="audio/x-pn-realaudio-plugin" '
                                   );
            $players['QT']  = array('id' => 'QuickTime',
                                    'clsid' => 'classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" ',
                                    'codebase' => 'codebase="http://www.apple.com/qtactivex/qtplugin.cab" ',
                                    'mime' => 'type="video/x-quicktime" '
                                   );
            $players['SWF'] = array('id' => 'SWFlash',
                                    'clsid' => ' classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" ',
                                    'codebase' => '',
                                    'mime' => 'type="application/x-shockwave-flash" ',
                                    'data' => 'data="'.$picture_url.'" '
                                   );
            $players['UNK'] = array('id' => 'DefaultPlayer',
                                    'clsid' => '',
                                    'codebase' => '',
                                    'mime' => ''
                                   );

            $player = $players[$mime_content['player']];

            if (!$player) {
                $player = 'UNK';
            }

            $pic_html  = '<object id="'.$player['id'].'" '.$player['data'].$player['classid'].$player['codebase'].$player['mime'].$image_size['whole'].'>';
            $pic_html .= "<param name=\"autostart\" value=\"$autostart\" /><param name=\"src\" value=\"". $picture_url . "\" />";
            $pic_html .= '</object><br />' . $LINEBREAK;
        }

        //PLUGIN FILTER
        $pic_html = CPGPluginAPI::filter('html_other_media', $pic_html);
    }

    $CURRENT_PIC_DATA['html'] = $pic_html;
    $CURRENT_PIC_DATA['header'] = '';
    $CURRENT_PIC_DATA['footer'] = '';

    $CURRENT_PIC_DATA = CPGPluginAPI::filter('file_data',$CURRENT_PIC_DATA);

    $params = array('{CELL_HEIGHT}' => '100',
        '{IMAGE}' => $CURRENT_PIC_DATA['header'].$CURRENT_PIC_DATA['html'].$CURRENT_PIC_DATA['footer'],
        '{ADMIN_MENU}' => $CURRENT_PIC_DATA['menu'],
        '{TITLE}' => bb_decode($CURRENT_PIC_DATA['title']),
        '{CAPTION}' => bb_decode($CURRENT_PIC_DATA['caption']),
        );

    return template_eval($template_display_media, $params);
}
/******************************************************************************
** Section <<<theme_html_picture>>> - END
******************************************************************************/


